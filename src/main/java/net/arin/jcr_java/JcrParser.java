/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (c) 2013 Cogito Learning Ltd
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Recursive decent parser for JCR.
 *
 * This a parser and tokenizer are derivative of the tutorial code from Cogito Learning.
 * http://cogitolearning.co.uk/?p=523
 */
public class JcrParser
{
    /** the tokens to parse */
    ArrayList<Token> tokens;
    /** keeps track of where we are in the array list */
    int tokenIdx;
    /** the next token */
    Token lookahead;
    /** save points in the token stack */
    LinkedList<Integer> savePoints;
    /** saved tokens */
    ArrayList<Token> saved;
    /** the parsed objects stack */
    LinkedList<ParsedObject> parsedObjects;
    /** parsing rules */
    HashMap<String, NonTerminal> parsingRules;
    /** last successful parsing point message */
    String lastSuccessfulPointMsg = null;
    /** last successfully parsed token */
    Token lastSuccessfulPoint = null;

    Logger log = Logger.getLogger( JcrParser.class.getName() );

    void setupParse( ArrayList<Token> tokens )
    {
        this.tokens = tokens;
        this.savePoints = new LinkedList<>();
        tokenIdx = 0;
        lookahead = this.tokens.get( tokenIdx );
        saved = new ArrayList<>();
        parsedObjects = new LinkedList<>();
        parsingRules = new HashMap<>();
    }

    public static RuleSet parse( String s )
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( s );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        return jcrParser.parse( tokens );
    }

    public RuleSet parse(ArrayList<Token> tokens)
    {
        if( log.isLoggable( Level.FINER ) )
        {
            StringJoiner sj = new StringJoiner( " " );
            tokens.forEach( (t) -> sj.add( t.toString() ) );
            log.finer( "parsed tokens " + sj.toString() );
        }
        setupParse( tokens );
        NonTerminals.setupParsingRules( this );

        RuleSet ruleSet = new RuleSet();
        push( ruleSet );
        NonTerminal.ntRule("jcr" ).parse(this );

        if (lookahead.tokenType != TokenType.END_OF_FILE )
        {
            Token t = lookahead;
            if( lastSuccessfulPoint != null )
            {
                t = lastSuccessfulPoint;
            }
            String msg = "Unexpected symbol found at %s";
            if( lastSuccessfulPointMsg != null )
            {
                msg = lastSuccessfulPointMsg;
            }
            throw new ParsingException(msg, t);
        }

        return ruleSet;
    }

    void push( ParsedObject parsedObject )
    {
        parsedObjects.push( parsedObject );
    }

    ParsedObject pop()
    {
        return parsedObjects.pop();
    }

    public ParsedObject peek()
    {
        return parsedObjects.peek();
    }

    public ParsedObject popAndConsume()
    {
        ParsedObject retval = pop();
        peek().accept( retval );
        return retval;
    }

    void newNtRule(String name, NonTerminal parsingRule )
    {
        if( parsingRules.get( name ) != null )
        {
            throw new ParsingException( "parsing rule " + name + " already defined." );
        }
        parsingRules.put( name, parsingRule );
    }

    NonTerminal ntRule(String name )
    {
        NonTerminal retval = parsingRules.get( name );
        if( retval == null )
        {
            throw new ParsingException( "parsing rule '" + name + "' not found" );
        }
        //else
        return retval;
    }

    /**
     * Remove the first token from the list and store the next token in lookahead
     */
    void nextToken()
    {
        tokenIdx++;
        // at the end of input we return an epsilon token
        if (tokenIdx >= tokens.size())
        {
            lookahead = new Token( TokenType.END_OF_FILE, "", -1, -1 );
        }
        else
        {
            lookahead = tokens.get( tokenIdx );
        }
    }

    /**
     * Used to save a place in the token stack.
     */
    void savePoint()
    {
        savePoints.push( tokenIdx );
        saved.clear();
    }

    /**
     * Used to jump back to the previous save point in the token stack.
     */
    void restorePoint()
    {
        tokenIdx = savePoints.pop();
        if( tokenIdx < tokens.size() )
        {
            lookahead = tokens.get( tokenIdx );
        }
        else
        {
            throw new ParsingException( "unexpected save of end input" );
            //lookahead = new Token( TokenType.END_OF_FILE, "", -1, -1 );
        }
    }

    /**
     * Used to abandon a save point.
     */
    void removeSavePoint()
    {
        savePoints.pop();
    }

    void handleSavePoint( boolean remove )
    {
        if( remove )
        {
            removeSavePoint();
        }
        else
        {
            restorePoint();
        }
    }

    /**
     * Convenience method for dealing with removing or restoring save points.
     * @param parsedObject if null then restore, else remove
     */
    void handleSavePoint( ObjectRule parsedObject )
    {
        handleSavePoint( parsedObject != null );
    }

    /**
     * Expects that {@link JcrParser#lookahead} is the correct token type
     * otherwise throw a parsing error
     * @param tokenType is the {@link TokenType} to compare against
     */
    void expectToken( TokenType tokenType )
    {
        if( lookahead.tokenType != tokenType )
        {
            throw new ParsingException("Unexpected symbol %s found", lookahead);
        }
        else
        {
            nextToken();
        }
    }

    /**
     * Passed into {@link #sequence(Repeat, ParseItem...)} and {@link #choice(Repeat, ParseItem...)}
     * to signify how often repetition is to occur.
     */
    enum Repeat{
        OPTIONAL(0,1),
        ONCE(1,1),
        ONE_OR_MORE(1,Integer.MAX_VALUE),
        ZERO_OR_MORE(0,Integer.MAX_VALUE);

        int min;
        int max;

        @Override
        public String toString()
        {
            return "Repeat{" +
                "min=" + min +
                ", max=" + max +
                '}';
        }

        Repeat( int min, int max )
        {
            this.min = min;
            this.max = max;
        }
    }

    /**
     * Parses a sequence of tokens and non-terminals.
     * @param repeat how often the sequence is repeated
     * @param parseItems the tokens and non-terminals to parse
     * @return
     */
    boolean sequence( Repeat repeat, ParseItem... parseItems )
    {
        return sequence( repeat, null, parseItems );
    }

    static NonTerminal pSeq(Repeat repeat, ParseItem... parseItems )
    {
        return (p) -> p.sequence( repeat, parseItems );
    }

    static NonTerminal pSeq(ParseItem... parseItems )
    {
        return pSeq(Repeat.ONCE, parseItems );
    }

    static NonTerminal pMaybe( ParseItem... parseItems )
    {
        return (p) -> p.sequence( Repeat.OPTIONAL, parseItems );
    }

    /**
     * Parses a sequence of tokens and non-terminals.
     * @param repeat how often the sequence is repeated.
     * @param onSuccess a function to execute for every successful repetition
     * @param parseItems the tokens and non-terminals to parse
     * @return
     */
    boolean sequence( Repeat repeat, Consumer<ArrayList<Token>> onSuccess, ParseItem... parseItems )
    {
        boolean logMatched = false;
        int success = 0;
        for( int i = 0; i < repeat.max; i++ )
        {
            int startTokenIdx = tokenIdx;
            boolean found = false;
            // don't continue if there are no more tokens to parse
            if( lookahead.tokenType == TokenType.END_OF_FILE )
            {
                break;
            }
            savePoint();
            for ( ParseItem parseItem : parseItems )
            {
                found = parseParseItem( parseItem );
                if( !found )
                {
                    break;
                }
            }
            handleSavePoint( found );
            if( found )
            {
                success++;
                logMatched = logMatched || logTokens( tokens.subList( startTokenIdx, tokenIdx ),
                    Level.FINER, "sequence matched tokens: " );
                if( onSuccess != null )
                {
                    onSuccess.accept( saved );
                }
            }
            else
            {
                break;
            }
        }
        boolean retval = true;
        if( success < repeat.min )
        {
            retval = false;
        }
        else if( success > repeat.max )
        {
            retval = false;
        }
        logParseItems( logMatched, "SEQUENCE", success, retval, repeat, parseItems );
        return retval;
    }

    /**
     * Parses a choice of tokens or non-terminals.
     * @param repeat how often the choice is to repeat
     * @param parseItems the tokens and non-terminals to parse
     * @return
     */
    boolean choice( Repeat repeat, ParseItem... parseItems )
    {
        return choice( repeat, null, parseItems );
    }

    static NonTerminal pChoice( Repeat repeat, ParseItem... parseItems )
    {
        return (p) -> p.choice( repeat, parseItems );
    }

    static NonTerminal pChoice( ParseItem... parseItems )
    {
        return pChoice( Repeat.ONCE, parseItems );
    }

    /**
     * Parses a choice of tokens or non-terminals.
     * @param repeat how often the choice is to repeat
     * @param onSuccess function to execute for every successful repetition
     * @param parseItems the tokens and non-terminals to parse
     * @return
     */
    boolean choice( Repeat repeat, Consumer<ArrayList<Token>> onSuccess, ParseItem... parseItems )
    {
        boolean logMatched = false;
        int success = 0;
        for( int i = 0; i < repeat.max; i++ )
        {
            boolean found = false;
            int startTokenIdx = tokenIdx;
            // don't continue if there are no more tokens to parse
            if( lookahead.tokenType == TokenType.END_OF_FILE )
            {
                break;
            }
            savePoint();
            for ( ParseItem parseItem : parseItems )
            {
                found = parseParseItem( parseItem );
                if( found )
                {
                    break;
                }
            }
            handleSavePoint( found );
            if( found )
            {
                success++;
                logMatched = logMatched || logTokens( tokens.subList( startTokenIdx, tokenIdx ),
                    Level.FINER, "choice matched tokens: " );
                if( onSuccess != null )
                {
                    onSuccess.accept( saved );
                }
            }
            else
            {
                break;
            }
        }
        boolean retval = true;
        if( success < repeat.min )
        {
            retval = false;
        }
        else if( success > repeat.max )
        {
            retval = false;
        }
        logParseItems( logMatched, "CHOICE", success, retval, repeat, parseItems );
        return retval;
    }

    /**
     * Calls a {@link BiConsumer} when items are parsed successfully.
     * @param biConsumer the function called
     * @param parseItems the items to parse
     * @return true if there was a parsing match.
     */
    boolean consume( BiConsumer<ArrayList<Token>, JcrParser> biConsumer, ParseItem... parseItems )
    {
        int startIdx = tokenIdx;
        boolean retval = sequence( Repeat.ONCE, parseItems );
        if( retval )
        {
            ArrayList<Token> unignorableTokens = new ArrayList<>();
            tokens.subList( startIdx, tokenIdx ).forEach( unignorableTokens::add );
            logTokens( unignorableTokens, Level.FINER, "consumed tokens: " );
            biConsumer.accept( unignorableTokens, this );
        }
        return retval;
    }

    static NonTerminal pConsume( BiConsumer<ArrayList<Token>, JcrParser> collector, ParseItem... parseItems)
    {
        return (p) -> p.consume( collector, parseItems );
    }

    private boolean parseParseItem( ParseItem parseItem )
    {
        boolean retval;
        if( parseItem instanceof TokenType )
        {
            retval = (lookahead.tokenType == parseItem);
            if( retval )
            {
                if( ! lookahead.tokenType.isIgnorable() )
                {
                    saved.add( lookahead );
                }
                nextToken();
            }
        }
        else //must be a NonTerminal
        {
            retval = ((NonTerminal)parseItem).parse( this );
        }
        return retval;
    }

    boolean logTokens( List<Token> matched, Level level, String message )
    {
        Token starting = null;
        for ( Token token : matched )
        {
            if( !token.tokenType.isIgnorable() )
            {
                starting = token;
                break;
            }
        }
        if( starting != null )
        {
            StringJoiner seqSj = new StringJoiner( " ", "'", "'" );
            StringJoiner ttSj = new StringJoiner( " " );
            matched.forEach( (t) -> {
                if( ! t.tokenType.isIgnorable() )
                {
                    seqSj.add( t.sequence );
                    ttSj.add( t.tokenType.toString() );
                }
            } );
            log.log( level, message + " starting at " + starting.line + "," + starting.col +
                " sequence: " + seqSj.toString() + " token types: " + ttSj.toString() );
            return true;
        }
        return false;
    }

    private void logParseItems( boolean logMatched, String kind, int successes, boolean retval, Repeat repeat, ParseItem... parseItems )
    {

        StringJoiner sj = new StringJoiner( ", " );
        sj.add( kind ).add( repeat.toString() ).add( "found: " + successes).add( "returning: " + retval );
        StringJoiner piSj = new StringJoiner( ", ", "[", "]" );
        for ( ParseItem parseItem : parseItems )
        {
            piSj.add( parseItem.toString() );
        }
        sj.add( piSj.toString() );
        if( logMatched )
        {
            log.finer( sj.toString() );
        }
        else
        {
            log.finest( sj.toString() );
        }
    }

}
