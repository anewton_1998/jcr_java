/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import java.util.ArrayList;

/**
 * Types of combiner
 */
public enum Combiner implements ParsedObject
{
    SEQUENCE, CHOICE;



    public String toWord()
    {
        String retval = null;
        switch( this )
        {
            case SEQUENCE:
                retval = "AND";
                break;
            case CHOICE:
                retval = "OR";
                break;
            default:
                throw new ParsingException( "unkonwn combiner type" );
        }
        return retval;
    }

    static Combiner combiner( Token token )
    {
        if( token.tokenType == TokenType.PIPE )
        {
            return CHOICE;
        }
        else if( token.tokenType == TokenType.COMMA )
        {
            return SEQUENCE;
        }
        throw new ParsingException( "token %s is not a combiner", token );
    }

    static Combiner combiner(ArrayList<Token> tokens )
    {
        Combiner retval = null;
        for (Token token : tokens) {
            if( token.tokenType == TokenType.SPACES || token.tokenType == TokenType.COMMENT )
            {
                continue;
            }
            else if( retval == null )
            {
                retval = combiner( token );
            }
            else
            {
                throw new ParsingException( "token %s cannot be parsed as a combiner", token );
            }
        }
        return retval;
    }

    @Override
    public void accept( ParsedObject parsedObject )
    {
        throw new InternalError( "combiners don't accept anything", parsedObject );
    }

    @Override
    public String toString()
    {
        String retval = null;
        switch( this )
        {
            case SEQUENCE:
                retval = ",";
                break;
            case CHOICE:
                retval = "|";
                break;
            default:
                throw new ParsingException( "unkonwn combiner type" );
        }
        return retval;
    }
}
