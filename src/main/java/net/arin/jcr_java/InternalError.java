/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (c) 2013 Cogito Learning Ltd
 * Copyright (c) 2016-2017 ARIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package net.arin.jcr_java;

/**
 * Used for bad things that shouldn't happen.
 */
public class InternalError extends RuntimeException
{
    /** the token that caused the error */
    private Token token = null;
    private ParsedObject parsedObject = null;

    /**
     * Construct the evaluation exception with a message.
     * @param message the message containing the cause of the exception
     */
    public InternalError( String message)
    {
        super(message);
    }

    /**
     * Construct the evaluation exception with a message and a token.
     * @param message the message containing the cause of the exception
     * @param token the token that caused the exception
     */
    public InternalError( String message, Token token)
    {
        super(message);
        this.token = token;
    }

    public InternalError( String message,
                          ParsedObject parsedObject )
    {
        super( message );
        this.parsedObject = parsedObject;
    }

    /**
     * Get the token.
     * @return the token that caused the exception
     */
    public Token getToken()
    {
        return token;
    }

    public ParsedObject getParsedObject()
    {
        return parsedObject;
    }

    /**
     * Overrides RuntimeException.getMessage to add the token information
     * into the error message.
     *
     *  @return the error message
     */
    public String getMessage()
    {
        String msg = super.getMessage();
        if( msg.contains( "%s" ) )
        {
            if (token != null)
            {
                msg = msg.replace("%s", token.tokenType.toString() );
            }
            else if( parsedObject != null )
            {
                msg = msg.replace("%s", parsedObject.getClass().getName() );
            }
        }
        else
        {
            if( token != null )
            {
                msg = msg + ": " + token.toString();
            }
            else if( parsedObject != null )
            {
                msg = msg + ": " + parsedObject.toString();
            }
        }
        return msg;
    }

}