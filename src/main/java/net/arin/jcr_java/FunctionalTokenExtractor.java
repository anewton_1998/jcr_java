/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

/**
 * An class that uses a functional pointer for extraction.
 */
public class FunctionalTokenExtractor implements TokenExtractor
{
    private TokenType tokenType;
    private TokenExtraction tokenExtraction;

    public FunctionalTokenExtractor( TokenType tokenType,
                                     TokenExtraction tokenExtraction )
    {
        this.tokenType = tokenType;
        this.tokenExtraction = tokenExtraction;
    }

    public FunctionalTokenExtractor()
    {
    }

    @Override
    public TokenType getTokenType()
    {
        return tokenType;
    }

    @Override
    public void setTokenType( TokenType tokenType )
    {
        this.tokenType = tokenType;
    }

    public TokenExtraction getTokenExtraction()
    {
        return tokenExtraction;
    }

    public void setTokenExtraction( TokenExtraction tokenExtraction )
    {
        this.tokenExtraction = tokenExtraction;
    }

    @Override
    public String extract( String source )
    {
        return tokenExtraction.extract( source );
    }
}
