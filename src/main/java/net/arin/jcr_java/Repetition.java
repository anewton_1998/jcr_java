/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import java.util.ArrayList;

/**
 * Represents a JCR repetition
 */
public class Repetition implements ParsedObject
{
    int min = 1;
    int max = 1;
    int step = 1;

    @Override
    public void accept( ParsedObject parsedObject )
    {
        throw new InternalError( "Repetitions have no subordinate parsed objects" );
    }

    public Repetition()
    {
    }

    public Repetition( ArrayList<Token> tokens )
    {
        switch ( tokens.get( 0 ).tokenType )
        {
            case QUESTION_MARK:
                min = 0;
                break;
            case PLUS:
                min = 1;
                max = Integer.MAX_VALUE;
                step = step( tokens, 1 );
                break;
            case ASTERISK:
                if( tokens.size() == 1 )
                {
                    min = 0;
                    max = Integer.MAX_VALUE;
                }
                else
                {
                    switch( tokens.get( 1 ).tokenType )
                    {
                        case ZERO:
                        case POS_INTEGER:
                            min = Integer.valueOf( tokens.get( 1 ).sequence );
                            if( tokens.size() > 2 )
                            {
                                if( tokens.get( 2 ).tokenType == TokenType.DOUBLEDOT )
                                {
                                    if( tokens.size() >= 4 && tokens.get( 3 ).tokenType == TokenType.POS_INTEGER )
                                    {
                                        max = Integer.valueOf( tokens.get( 3 ).sequence );
                                        step = step( tokens, 4 );
                                    }
                                    else
                                    {
                                        max = Integer.MAX_VALUE;
                                        step = step( tokens, 3 );
                                    }
                                }
                                else
                                {
                                    max = Integer.valueOf( tokens.get( 1 ).sequence );
                                    step = step( tokens, 2 );
                                }
                            }
                            else
                            {
                                max = Integer.valueOf( tokens.get( 1 ).sequence );
                            }
                            break;
                        case DOUBLEDOT:
                            min = 0;
                            if( tokens.size() > 2 )
                            {
                                if( tokens.get( 2 ).tokenType == TokenType.POS_INTEGER )
                                {
                                    max = Integer.valueOf( tokens.get( 2 ).sequence );
                                }
                                else
                                {
                                    throw new InternalError( "wrong max repetition token", tokens.get( 2 ) );
                                }
                                step = step( tokens, 3 );
                            }
                            else
                            {
                                throw new InternalError( "no max repetition token" );
                            }
                            break;
                        case PERCENT:
                            min = 0;
                            max = Integer.MAX_VALUE;
                            step = step( tokens, 1 );
                            break;
                        default:
                            throw new InternalError( "unknown token in repetition", tokens.get( 1 ) );
                    }
                }
                break;
            default:
                throw new InternalError( "unknown token in repetition", tokens.get( 0 ) );
        }
    }

    private int step( ArrayList<Token> tokens, int index )
    {
        int retval = 1;
        if( tokens.size() >= index+1 )
        {
            if( tokens.get( index ).tokenType == TokenType.PERCENT )
            {
                if( tokens.size() == index + 2 && tokens.get( index + 1 ).tokenType == TokenType.POS_INTEGER )
                {
                    retval = Integer.valueOf( tokens.get( index + 1 ).sequence );
                }
                else
                {
                    throw new InternalError( "either no token or wrong token for repetition step" );
                }
            }
            else
            {
                throw new InternalError( "step qualifier token is wrong type %s", tokens.get( index) );
            }
        }
        return retval;
    }

    @Override
    public String toString()
    {
        String retval = "";
        if( min == 1 && max == 1 )
        {
            retval = "";
        }
        else if( min == 0 && max == 1 )
        {
            retval = " ?";
        }
        else if( min == 0 && max == Integer.MAX_VALUE )
        {
            if( step > 1 )
            {
                retval = " *%" + step;
            }
            else
            {
                retval = " *";
            }
        }
        else if( min == 1 && max == Integer.MAX_VALUE )
        {
            if( step > 1 )
            {
                retval = " +%" + step;
            }
            else
            {
                retval = " +";
            }
        }
        else
        {
            if( step > 1 )
            {
                retval = " *" + min + ".." + max + "%" + step;
            }
            else
            {
                retval = " *" + min + ".." + max;
            }
        }
        return retval;
    }

    public String toWords()
    {
        return "REPEAT " +
            min +
            ".." + max +
            " STEP " + step;
    }
}
