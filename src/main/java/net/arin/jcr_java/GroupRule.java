/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import java.util.ArrayList;
import java.util.StringJoiner;
import java.util.logging.Logger;

/**
 * Represents a group rule.
 */
public class GroupRule implements ParsedObject
{
    Logger log = Logger.getLogger( getClass().getName() );

    Annotation not;
    Annotation root;

    ArrayList<ParsedObject> groupSubordinates;

    public GroupRule()
    {
        groupSubordinates = new ArrayList<>();
    }

    @Override
    public String toString()
    {
        StringJoiner sj = new StringJoiner(" ", " ", " " );
        sj.setEmptyValue( " " );
        groupSubordinates.forEach( (s) -> sj.add( s.toString() ) );
        return Annotation.toString( root, not ) + "(" +
               sj.toString() + ")";
    }

    @Override
    public void accept(ParsedObject parsedObject)
    {
        if( parsedObject instanceof Annotation )
        {
            Annotation a = (Annotation) parsedObject;
            switch(a)
            {
                case NOT:
                    not = a;
                    break;
                case ROOT:
                    root = a;
                    break;
                case UNORDERED:
                    throw new ParsingException( "@{unordered} is not applicable to group rules" );
                default:
                    log.warning( "unknown annotation applied to group: " + a.getToken() );
            }
        }
        else
        {
            if( parsedObject instanceof ContainedItem )
            {
                log.fine( "adding subordinate rule to group" );
                groupSubordinates.add( parsedObject );
            }
            else
            {
                throw new InternalError( "parsed object of type %s cannot be applied to a group subordinate.", parsedObject );
            }
        }
    }

    @Override
    public ParsedObject getSub(int index)
    {
        return groupSubordinates.get( index );
    }
}

