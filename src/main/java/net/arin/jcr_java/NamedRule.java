/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Represents a rule with a name
 */
public class NamedRule implements ParsedObject
{
    Logger log = Logger.getLogger( getClass().getName() );

    Annotation not = null;
    Annotation root = null;
    ParsedObject specification;
    String name;

    public NamedRule( ArrayList<Token> tokens )
    {
        if( tokens.get( 0 ).tokenType == TokenType.NAME )
        {
            name = tokens.get( 0 ).sequence;
        }
        else
        {
            throw new InternalError( "this doesn't look like a named rule", tokens.get( 1 ) );
        }
    }

    @Override
    public void accept( ParsedObject parsedObject )
    {
        if( parsedObject instanceof Annotation )
        {
            Annotation a = (Annotation) parsedObject;
            if( a == Annotation.NOT )
            {
                not = a;
            }
            else if ( a == Annotation.ROOT )
            {
                root = a;
            }
            else
            {
                log.warning( "Unkownn annotation applied to named rule: " + a.getToken() );
            }
        }
        else if(parsedObject instanceof ArrayRule ||
                parsedObject instanceof PrimitiveRule ||
                parsedObject instanceof MemberRule ||
                parsedObject instanceof ObjectRule ||
                parsedObject instanceof GroupRule ||
                parsedObject instanceof TargetRuleName )
        {
            specification = parsedObject;
        }
        else
        {
            throw new InternalError( "adding unkonwn parsed object to named rule", parsedObject );
        }
    }

    @Override
    public ParsedObject getSub( int index )
    {
        if( index == 0 )
        {
            return specification;
        }
        //else
        return null;
    }

    @Override
    public String toString()
    {
        if( specification instanceof PrimitiveRule)
        {
            return String.format( "%s$%s =: %s", Annotation.toString( not, root ), name, specification );
        }
        //else
        return String.format( "%s$%s = %s", Annotation.toString( not, root ), name, specification );
    }
}
