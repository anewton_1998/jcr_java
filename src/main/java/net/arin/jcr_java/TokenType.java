/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (c) 2013 Cogito Learning Ltd
 * Copyright (c) 2016-2017 ARIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package net.arin.jcr_java;

/**
 * Represents the types of tokens.
 */
public enum TokenType implements ParseItem
{
    /**
     * comment = ";" *comment-char comment-end-char
     *           comment-char = HTAB / %x20-10FFFF
     *            ; Any char other than CR / LF
     *            comment-end-char = CR / LF
     *
     * We are gonna cheat and do this at the tokenizer where it can be ignored easily
     * in the interpretation layer of the parser. Because the tokenzier is removing
     * \r\n we just eat the all characters after ;
     */
    COMMENT( new RegexTokenExtractor( ";.*" ), true ),

    //must come before INT_KW
    INTEGER_KW( "integer", false ),

    //sized int/uint keywords
    INT_KW( "int", false ),
    UINT_KW( "uint", false ),

    //primitive standalone keywords
    STRING_KW( "string", false ),
    NULL_KW( "null", false ),
    BOOLEAN_KW( "boolean", false ),
    TRUE_KW( "true", false ),
    FALSE_KW( "false", false ),
    FLOAT_KW( "float", false ),
    DOUBLE_KW( "double", false ),

    IPV4_KW( "ipv4", false ),
    IPV6_KW( "ipv6", false ),
    IPADDR_KW( "ipaddr", false ),
    PHONE_KW( "phone", false ),
    EMAIL_KW( "email", false ),

    DATETIME_KW( "datetime", false ),
    DATE_KW( "date", false ),
    TIME_KW( "time", false ),

    HEX_KW( "hex", false ),
    BASE32HEX_KW( "base32hex", false ),
    BASE32_KW( "base32", false ),
    BASE64URL_KW( "base64url", false ),
    BASE64_KW( "base64", false ),

    FQDN_KW( "fqdn", false ),
    IDN_KW( "idn", false ),

    /**
     * Must come before {@link #URI_KW}
     * uri_range = uri-dotdot-kw uri_scheme
     * uri-dotdot-kw = "uri.."
     * uri_scheme = 1*ALPHA
     */
    URI_RANGE( new RegexTokenExtractor( "uri\\.\\.\\p{Alpha}+" ), false ),
    URI_KW( "uri", false ),

    ANY_KW( "any", false ),
    JCR_VERSION_KW( "jcr-version", false ),
    AS_KW( "as", false ),
    IMPORT_KW( "import", false ),
    RULESET_ID_KW( "ruleset-id", false ),

    /**
     * not-kw = "not"
     */
    NOT_KW( "not", false ),
    /**
     * root_kw = "root"
     */
    ROOT_KW( "root", false ),
    /**
     * unordered-kw = "unordered"
     */
    UNORDERED_KW( "unordered", false ),

    /**
     * type-kw = "type"
     */
    TYPE_KW( "type", false ),

    AT_SIGN( "@", false ),
    LEFT_CURLY( "{", false ),
    RIGHT_CURLY( "}", false ),
    LEFT_PARENTHESIS( "(", false ),
    RIGHT_PARENTHESIS( ")", false ),
    OCTOTHORPE( "#", false ),

    /*
     * An example of doing this with an inline function
     * left here for reference if something like this is needed later.
     */
    LEFT_BRACKET( (s) -> s.startsWith( "[" ) ? "[" : null , false ),

    /*
     * An example of doing this with a function reference
     * left here for reference if something like this is needed later.
     */
    RIGHT_BRACKET( TokenType::rightBracket, false ),

    COMMA( ",", false ),
    PIPE( "|", false ),

    /**
     *
     * name              = ALPHA *( ALPHA / DIGIT / "-" / "_" )
     */
    NAME( new RegexTokenExtractor( "\\p{Alpha}[\\p{Alnum}-_]*" ), false ),

    /**
     * any token than begins with an alphabetic character
     */
    ALPHATOKEN( new RegexTokenExtractor( "\\p{Alpha}\\S*" ), false ),

    /**
     *
     * rule(:regex)     { str('/') >> (str('\\/') | match('[^/]+')).repeat.as(:regex) >> str('/') >> regex_modifiers.maybe }
     * regex = "/" *( escape "/" / not-slash ) "/" [ regex_modifiers ]
     * not-slash = HTAB / CR / LF / %x20-2E / %x30-10FFFF
     *             ; Any char except "/"
     * rule(:regex_modifiers) { match('[isx]').repeat.as(:regex_modifiers) }
     * regex_modifiers = *( "i" / "s" / "x" )
     */
    REGEX( new RegexTokenExtractor( "\\/((\\u0022\\/)|[\\t\\r\\n\\u0020-\\u002E\\u0030-\\u10FFFF])*\\/(i|s|x)?" ), false ),


    /**
     * Since float is not used elsewhere in the syntax, we don't break it out.
     *
     * float         = [ minus ] int frac [ exp ]
     *                 ; From RFC 7159 except 'frac' required
     * minus         = %x2D                          ; -
     * plus          = %x2B                          ; +
     * int           = zero / ( digit1-9 *DIGIT )
     * digit1-9      = %x31-39                       ; 1-9
     * frac          = decimal-point 1*DIGIT
     * decimal-point = %x2E                          ; .
     * exp           = e [ minus / plus ] 1*DIGIT
     * e             = %x65 / %x45                   ; e E
     * zero          = %x30                          ; 0
     */
    FLOAT( new RegexTokenExtractor( "-?[0-9]+\\.[0-9]+(e(-|\\+)[1-9][1-9]*)?" ), false ),

    /**
     * Since pos_integer and non_neg_integer are used in other parts of
     * the syntax (repetitions), we put the pos_integer, zero, and minus
     * into the tokenizer and integer and non_neg_integer as parsing rules.
     *
     * pos_integer = digit1-9 *DIGIT
     */
    POS_INTEGER( new RegexTokenExtractor( "[1-9][0-9]*" ), false ),

    /**
     * Since pos_integer and non_neg_integer are used in other parts of
     * the syntax (repetitions), we put the pos_integer, zero, and minus
     * into the tokenizer and integer and non_neg_integer as parsing rules.
     */
    ZERO( "0", false ),

    /**
     * Since pos_integer and non_neg_integer are used in other parts of
     * the syntax (repetitions), we put the pos_integer, zero, and minus
     * into the tokenizer and integer and non_neg_integer as parsing rules.
     */
    MINUS( "-", false ),

    /**
     * Double periods used ranges.
     */
    DOUBLEDOT( "..", false ),

    COLLON( ":", false ),
    QUESTION_MARK( "?", false ),
    PLUS( "+", false ),
    ASTERISK( "*", false ),
    PERCENT( "%", false ),
    DOLLAR( "$", false ),
    PERIOD( ".", false ),
    EQUALS_SIGN( "=", false ),

    /**
     * For the purposes of parsing, we just want to look for a quoted string.
     * The interpretation of escaped values comes later.
     *
     * q_string = quotation-mark *char quotation-mark
     *            ; From RFC 7159
     * char = unescaped /
     *   escape (
     *   %x22 /          ; "    quotation mark  U+0022
     *   %x5C /          ; \    reverse solidus U+005C
     *   %x2F /          ; /    solidus         U+002F
     *   %x62 /          ; b    backspace       U+0008
     *   %x66 /          ; f    form feed       U+000C
     *   %x6E /          ; n    line feed       U+000A
     *   %x72 /          ; r    carriage return U+000D
     *   %x74 /          ; t    tab             U+0009
     *   %x75 4HEXDIG )  ; uXXXX                U+XXXX
     * escape = %x5C              ; \
     * quotation-mark = %x22      ; "
     * unescaped = %x20-21 / %x23-5B / %x5D-10FFFF
     */
    QSTRING( TokenType::qString, false ),

    /**
     * spaces = 1*( WSP / CR / LF )
     */
    SPACES( new RegexTokenExtractor( "\\s+" ), true ),

    /**
     * END OF FILE marker
     */
    END_OF_FILE( ( s) -> null, true )
    ;

    private TokenExtractor tokenExtractor;
    private boolean isIgnorable;

    public TokenExtractor getTokenExtractor()
    {
        return tokenExtractor;
    }

    /**
     * Determines if the token type is ultimately ignorable (i.e. white space, comments).
     * @return true if ignorable
     */
    public boolean isIgnorable()
    {
        return isIgnorable;
    }

    TokenType( TokenExtractor tokenExtractor, boolean isIgnorable )
    {
        this.tokenExtractor = tokenExtractor;
        tokenExtractor.setTokenType( this );
        this.isIgnorable = isIgnorable;
    }

    TokenType( TokenExtraction tokenExtraction, boolean isIgnorable )
    {
        this.tokenExtractor = new FunctionalTokenExtractor( this,
            tokenExtraction );
        this.isIgnorable = isIgnorable;
    }

    TokenType( String stringToExtract, boolean isIgnorable )
    {
        StringTokenExtractor stringTokenExtractor = new StringTokenExtractor( stringToExtract );
        stringTokenExtractor.setTokenType( this );
        this.tokenExtractor = stringTokenExtractor;
        this.isIgnorable = isIgnorable;
    }

    static String rightBracket( String source )
    {
        return source.startsWith( "]" ) ? "]" : null;
    }

    static String qString( String source )
    {
        boolean found = false;
        StringBuilder sb = new StringBuilder();
        if( source.charAt( 0 ) == '"' )
        {
            sb.append( source.charAt( 0 ) );
            int i = 1;
            boolean end = false;
            while( !end )
            {
                char c = source.charAt( i );
                if( c == '"' )
                {
                    sb.append( c );
                    if( source.charAt( i - 1 ) != '\\' )
                    {
                        end = true;
                        found = true;
                    }
                }
                else if( i >= source.length() )
                {
                    end = true;
                }
                else
                {
                    sb.append( c );
                }
                i++;
            }
        }
        if( found )
        {
            return sb.toString();
        }
        //else
        return null;
    }
}