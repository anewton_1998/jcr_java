/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import java.util.ArrayList;
import java.util.StringJoiner;

public class JcrVersionDirective  implements ParsedObject
{
    public static final float VERSION = 0.7f;

    private ArrayList<String> extensionIds;

    public JcrVersionDirective( ArrayList<Token> tokens )
    {
        // get and check the version number
        for( Token token : tokens )
        {
            if( token.tokenType == TokenType.FLOAT )
            {
                if(Float.valueOf( token.sequence ) != VERSION)
                {
                    throw new ParsingException( "JCR version must be " + VERSION + " but is specified as " + token.sequence );
                }
                //else
                break;
            }
        }

        extensionIds = new ArrayList<>();
        boolean nextTokenIsExtensionId = false;
        for(Token token : tokens)
        {
            if( token.tokenType == TokenType.PLUS )
            {
                nextTokenIsExtensionId = true;
            }
            else if( nextTokenIsExtensionId && token.tokenType != TokenType.SPACES )
            {
                extensionIds.add( token.sequence );
                nextTokenIsExtensionId = false;
            }
        }
    }

    @Override
    public String toString()
    {
        StringJoiner sj = new StringJoiner( " + ", " + ", "" );
        sj.setEmptyValue( "" );
        extensionIds.forEach(sj::add);
        return "# jcr-version " + VERSION + sj.toString();
    }

    @Override
    public void accept(ParsedObject parsedObject)
    {
        throw new InternalError( "jcr-version directive cannot accept %", parsedObject );
    }
}
