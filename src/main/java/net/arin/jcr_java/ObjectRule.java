/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import java.util.ArrayList;
import java.util.StringJoiner;
import java.util.logging.Logger;

/**
 * Represents an object rule
 */
public class ObjectRule implements ParsedObject
{
    Logger log = Logger.getLogger( getClass().getName() );

    Annotation not = null;
    Annotation root = null;

    ArrayList<ParsedObject> objectSubordinates;

    public ObjectRule()
    {
        objectSubordinates = new ArrayList<>();
    }

    @Override
    public String toString()
    {
        StringJoiner sj = new StringJoiner(" ", " ", " " );
        sj.setEmptyValue( " " );
        objectSubordinates.forEach( (s) -> sj.add( s.toString() ) );
        return Annotation.toString( root, not ) + "{" +
               sj.toString() + "}";
    }

    @Override
    public ParsedObject getSub(int index)
    {
        return objectSubordinates.get( index );
    }

    @Override
    public void accept(ParsedObject parsedObject)
    {
        if( parsedObject instanceof Annotation )
        {
            Annotation a = (Annotation) parsedObject;
            switch( a )
            {
                case NOT:
                    not = a;
                    break;
                case ROOT:
                    root = a;
                    break;
                case UNORDERED:
                    throw new ParsingException( "@{unordered} is not applicable to object rules" );
                default:
                    log.warning( "unknown annotation applied to object: " + a.getToken() );

            }
        }
        else
        {
            if( parsedObject instanceof ContainedItem )
            {
                log.fine( "adding subordinate rule to object" );
                objectSubordinates.add( parsedObject );
            }
            else
            {
                throw new InternalError( "parsed object of type %s cannot be applied to an object subordinate.", parsedObject );
            }
        }
    }
}
