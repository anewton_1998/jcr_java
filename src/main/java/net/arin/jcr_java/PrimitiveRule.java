/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;


import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.logging.Logger;

/**
 * Represents a primitive specification.
 */
public class PrimitiveRule implements ParsedObject
{
    private Logger log = Logger.getLogger( getClass().getName() );

    private Predicate<ObjectRule> evaluator = new Predicate<ObjectRule>()
    {
        @Override
        public boolean test( ObjectRule o )
        {
            return false;
        }

        @Override
        public String toString()
        {
            return "Undefined PrimitiveRule";
        }
    };
    ArrayList<Token> tokens;

    Annotation not = null;
    Annotation root = null;

    @Override
    public void accept( ParsedObject parsedObject )
    {
        if( parsedObject instanceof Annotation )
        {
            Annotation a = (Annotation)parsedObject;
            if( a == Annotation.NOT )
            {
                not = a;
            }
            else if( a == Annotation.ROOT )
            {
                root = a;
            }
            else if( a == Annotation.UNORDERED )
            {
                throw new ParsingException( "unordered annotation does not apply to primitives." );
            }
            else
            {
                log.warning( "unknown annotation applied to primitive: " + a.getToken() );
            }
        }
        else
        {
            throw new InternalError( "primitives accept no subordiantes. type is %s", parsedObject );
        }
    }

    @Override
    public String toString()
    {
        return Annotation.toString( not, root ) + evaluator.toString();
    }

    public PrimitiveRule(ArrayList<Token> tokens )
    {
        this.tokens = tokens;
        Double dMax;
        Long lMax;
        switch( tokens.get( 0 ).tokenType )
        {
            case INTEGER_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public String toString()
                    {
                        return "integer";
                    }

                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }
                };
                break;
            case STRING_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public String toString()
                    {
                        return "string";
                    }

                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }
                };
                break;
            case NULL_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public String toString()
                    {
                        return "null";
                    }

                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }
                };
                break;
            case BOOLEAN_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public String toString()
                    {
                        return "boolean";
                    }

                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }
                };
                break;
            case TRUE_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public String toString()
                    {
                        return "true";
                    }

                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }
                };
                break;
            case FALSE_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public String toString()
                    {
                        return "false";
                    }

                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }
                };
                break;
            case FLOAT_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public String toString()
                    {
                        return "float";
                    }

                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }
                };
                break;
            case DOUBLE_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "double";
                    }
                };
                break;
            case IPV4_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public String toString()
                    {
                        return "ipv4";
                    }

                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }
                };
                break;
            case IPV6_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public String toString()
                    {
                        return "ipv6";
                    }

                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }
                };
                break;
            case IPADDR_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "ipaddr";
                    }
                };
                break;
            case PHONE_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public String toString()
                    {
                        return "phone";
                    }

                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }
                };
                break;
            case EMAIL_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "email";
                    }
                };
                break;
            case DATETIME_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "datetime";
                    }
                };
                break;
            case DATE_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "date";
                    }
                };
                break;
            case TIME_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "time";
                    }
                };
                break;
            case REGEX:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return tokens.get( 0 ).sequence;
                    }
                };
                break;
            case FLOAT:
                // 0.1
                if( tokens.size() == 1 )
                {
                    evaluator = new Predicate<ObjectRule>()
                    {
                        float v = Float.valueOf( tokens.get( 0 ).sequence );

                        @Override
                        public boolean test( ObjectRule o )
                        {
                            return false;
                        }

                        @Override
                        public String toString()
                        {
                            return Float.toString(v);
                        }
                    };
                }
                // 0.1..
                else if( tokens.size() == 2 && tokens.get( 1 ).tokenType == TokenType.DOUBLEDOT )
                {
                    float min = Float.valueOf( tokens.get( 0 ).sequence );
                    float max = Float.MAX_VALUE;
                    evaluator = new FloatRange( min, max );
                }
                // 0.1..1.1
                else if( tokens.size() == 3 &&
                    tokens.get( 1 ).tokenType == TokenType.DOUBLEDOT &&
                    tokens.get( 2 ).tokenType == TokenType.FLOAT )
                {
                    float min = Float.valueOf( tokens.get( 0 ).sequence );
                    float max = Float.valueOf( tokens.get( 2 ).sequence );
                    evaluator = new FloatRange( min, max );
                }
                else
                {
                    throw new InternalError( "unable to comprehend float token" );
                }
                break;
            case POS_INTEGER:
                // don't break. do the same thing as ZERO
            case ZERO:
                lMax = null;
                // '0' or '1'
                if( tokens.size() == 1 )
                {
                    evaluator = new IntegerValue( Long.valueOf( tokens.get( 0 ).sequence ) );
                }
                // '0..' or '1..'
                else if( tokens.size() == 2 &&
                    tokens.get( 1 ).tokenType == TokenType.DOUBLEDOT )
                {
                    evaluator = new IntegerRange( Long.valueOf( tokens.get( 0 ).sequence ), Long.MAX_VALUE );
                }
                // '0'..'2' or '1'..'2'
                else if( tokens.size() >=3 &&
                    tokens.get( 1 ).tokenType == TokenType.DOUBLEDOT &&
                    ( lMax = getIntFromTokens( tokens, 2 ) ) != null )
                {
                    evaluator = new IntegerRange( Long.valueOf( tokens.get( 0 ).sequence ), lMax );
                }
                else
                {
                    throw new InternalError( "unable to comprehend integer primitive" );
                }
                break;
            case MINUS:
                if( tokens.size() == 2 && tokens.get( 1 ).tokenType == TokenType.POS_INTEGER )
                {
                    evaluator = new IntegerValue( getIntFromTokens( tokens, 0 ) );
                }
                else if( tokens.size() >= 4 &&
                    tokens.get( 2 ).tokenType == TokenType.DOUBLEDOT &&
                    ( lMax = getIntFromTokens( tokens, 3 ) ) != null )
                {
                    evaluator = new IntegerRange( getIntFromTokens( tokens, 0 ), lMax );
                }
                else
                {
                    throw new InternalError( "unable to comprehend negative integer primitive" );
                }
                break;
            case DOUBLEDOT:
                lMax = null;
                if( tokens.size() == 1 )
                {
                    throw new InternalError( "number range does not have a maximum" );
                }
                else if( tokens.size() == 2 && tokens.get( 1 ).tokenType == TokenType.FLOAT )
                {
                    float max = Float.valueOf( tokens.get( 1 ).sequence );
                    evaluator = new FloatRange( Float.MIN_VALUE, max );
                }
                else if( tokens.size() >= 2 &&
                    (lMax = getIntFromTokens( tokens, 1 ) ) != null )
                {
                    evaluator = new IntegerRange( Integer.MIN_VALUE, lMax );
                }
                else
                {
                    throw new InternalError( "unable to comprehend numerical range" );
                }
                break;
            case QSTRING:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public String toString()
                    {
                        return tokens.get( 0 ).sequence;
                    }

                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }
                };
                break;
            case UINT_KW:
                if( tokens.size() == 2 &&
                    tokens.get( 1 ).tokenType == TokenType.POS_INTEGER )
                {
                    lMax = (long)Math.pow( 2, getIntFromTokens( tokens, 1 ) );
                    evaluator = new IntegerRange( 0, lMax, "UNSIGNED INT" );
                }
                else
                {
                    throw new InternalError( "unable to comprehend unsigned integer range" );
                }
                break;
            case INT_KW:
                if( tokens.size() == 2 &&
                    tokens.get( 1 ).tokenType == TokenType.POS_INTEGER )
                {
                    lMax = (long)Math.pow( 2, getIntFromTokens( tokens, 1 ) ) / 2;
                    evaluator = new IntegerRange( (lMax * -1l)+1l, lMax, "SIGNED INT" );
                }
                else
                {
                    throw new InternalError( "unable to comprehend signed integer range" );
                }
                break;
            case HEX_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "hex";
                    }
                };
                break;
            case BASE32HEX_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "base32hex";
                    }
                };
                break;
            case BASE32_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "base32";
                    }
                };
                break;
            case BASE64URL_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "base64url";
                    }
                };
                break;
            case BASE64_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "base64";
                    }
                };
                break;
            case ANY_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "any";
                    }
                };
                break;
            case FQDN_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "fqdn";
                    }
                };
                break;
            case IDN_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "idn";
                    }
                };
                break;
            case URI_KW:
                evaluator = new Predicate<ObjectRule>()
                {
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "uri";
                    }
                };
                break;
            case URI_RANGE:
                evaluator = new Predicate<ObjectRule>()
                {
                    String sequence = tokens.get( 0 ).sequence;
                    String scheme = sequence.substring( 5, sequence.length() );
                    @Override
                    public boolean test( ObjectRule o )
                    {
                        return false;
                    }

                    @Override
                    public String toString()
                    {
                        return "uri.." + scheme;
                    }
                };
                break;
            default:
                throw new InternalError( "unknown primitive token sequence" );

        }
    }

    class FloatRange implements Predicate<ObjectRule>
    {
        float min = Float.MIN_VALUE;
        float max = Float.MAX_VALUE;

        public FloatRange( float min, float max )
        {
            this.min = min;
            this.max = max;
        }

        @Override
        public boolean test( ObjectRule o )
        {
            return false;
        }

        @Override
        public String toString()
        {
            return min + ".." + max;
        }
    }

    class IntegerRange implements Predicate<ObjectRule>
    {
        long min = Long.MIN_VALUE;
        long max = Long.MAX_VALUE;
        String type = "INTEGER";

        public IntegerRange( long min, long max )
        {
            this.min = min;
            this.max = max;
        }

        public IntegerRange( long min, long max, String type )
        {
            this.min = min;
            this.max = max;
            this.type = type;
        }

        @Override
        public boolean test( ObjectRule o )
        {
            return false;
        }

        @Override
        public String toString()
        {
            return min + ".." + max;
        }
    }

    class IntegerValue implements Predicate<ObjectRule>
    {
        long v;

        public IntegerValue( long v )
        {
            this.v = v;
        }

        @Override
        public String toString()
        {
            return Long.toString(v);
        }

        @Override
        public boolean test( ObjectRule o )
        {
            return false;
        }
    }

    private Long getIntFromTokens( ArrayList<Token> tokens, int index )
    {
        Long retval = null;
        if( tokens.get( index ).tokenType == TokenType.ZERO )
        {
            retval = 0l;
        }
        else if( tokens.get( index ).tokenType == TokenType.MINUS )
        {
            long v = Long.valueOf( tokens.get( index + 1 ).sequence );
            retval = v * -1l;
        }
        else if( tokens.get( index ).tokenType == TokenType.POS_INTEGER )
        {
            retval = Long.valueOf( tokens.get( index ).sequence );
        }
        return retval;
    }
}
