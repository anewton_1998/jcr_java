/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import java.util.ArrayList;
import java.util.logging.Logger;

import static net.arin.jcr_java.TokenType.*;

/**
 * Represents a member rule.
 */
public class MemberRule implements ParsedObject
{
    Logger log = Logger.getLogger( getClass().getName() );

    Annotation not = null;

    // can be one but not both
    String memberRegex = null;
    String memberName = null;

    ParsedObject typeRule = null;

    public MemberRule(ArrayList<Token> tokens )
    {
        if( tokens.get( 0 ).tokenType == QSTRING )
        {
            memberName = tokens.get(0).sequence;
            log.fine( "member with name \"" + memberName + "\"");
        }
        else if( tokens.get(0).tokenType == REGEX )
        {
            memberRegex = tokens.get(0).sequence;
            log.fine( "member with regex /" + memberRegex + "/" );
        }
        if( memberName == null && memberRegex == null )
        {
            throw new ParsingException( "member rule must have either a member name or member regex" );
        }
    }

    @Override
    public void accept( ParsedObject parsedObject )
    {
        if( parsedObject instanceof Annotation )
        {
            Annotation a = (Annotation) parsedObject;
            if( a == Annotation.NOT )
            {
                not = a;
            }
            else //root and unordered are not applicable to member rules
            {
                log.warning( "Unkownn annotation applied to member rule: " + a.getToken() );
            }
        }
        else if(parsedObject instanceof ArrayRule ||
                parsedObject instanceof ObjectRule ||
                parsedObject instanceof GroupRule ||
                parsedObject instanceof PrimitiveRule )
        {
            typeRule = parsedObject;
        }
        else
        {
            throw new InternalError( "adding unkonwn parsed object to member rule", parsedObject );
        }
    }

    @Override
    public ParsedObject getSub( int index )
    {
        if( index == 0 )
        {
            return typeRule;
        }
        //else
        return null;
    }

    @Override
    public String toString()
    {
        String mns = memberRegex;
        if( mns == null )
        {
            mns = memberName;
        }
        return String.format( "%s%s : %s", Annotation.toString( not ), mns, typeRule );
    }
}
