/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import java.util.StringJoiner;

/**
 * Represents a JCR annotation.
 */
public enum Annotation implements ParsedObject
{
    NOT,
    ROOT,
    UNORDERED
    ;

    private Token token;

    public Token getToken()
    {
        return token;
    }

    public Annotation setToken( Token token )
    {
        this.token = token;
        return this;
    }

    @Override
    public String toString()
    {
        return "@{" + this.token.sequence + "}";
    }

    @Override
    public void accept( ParsedObject parsedObject )
    {
        throw new InternalError( "nothing should be calling this" );
    }

    static Annotation annotation( Token token )
    {
        if( token.tokenType == TokenType.NOT_KW )
        {
            return NOT.setToken( token );
        }
        else if( token.tokenType == TokenType.ROOT_KW )
        {
            return ROOT.setToken( token );
        }
        else if( token.tokenType == TokenType.UNORDERED_KW )
        {
            return UNORDERED.setToken( token );
        }
        //else
        return null;
    }

    static String toString( Annotation... annotations )
    {
        StringJoiner sj = new StringJoiner( " ", "", " " );
        sj.setEmptyValue( "" );
        for ( Annotation annotation : annotations )
        {
            if( annotation != null )
            {
                sj.add( annotation.toString() );
            }
        }
        return sj.toString();
    }
}
