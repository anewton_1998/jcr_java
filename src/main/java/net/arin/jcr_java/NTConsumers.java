/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import java.util.ArrayList;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import static net.arin.jcr_java.Annotation.annotation;

/**
 * A bunch of biconsumer functions for consuming parsed objects.
 */
public class NTConsumers
{
    static Logger log = Logger.getLogger( NTConsumers.class.getName() );

    static BiConsumer<ArrayList<Token>, JcrParser> newArrayConsumer = (t,p) ->
    {
        p.logTokens( t, Level.FINE, "new arrayRule " );
        ArrayRule arrayRule = new ArrayRule();
        //get any annotations on the stack
        while( p.peek() instanceof Annotation )
        {
            //remove the annotation and apply it to the arrayRule
            Annotation a = (Annotation) p.pop();
            arrayRule.accept(a );
        }
        //put the arrayRule on the stack
        p.push(arrayRule);
    };

    static BiConsumer<ArrayList<Token>, JcrParser> arrayItemConsumer = (t,p) ->
    {
        p.logTokens( t, Level.FINE, "new array item " );
        ContainedItem containedItem = new ContainedItem();
        ParsedObject po = p.pop();
        if( po instanceof Repetition )
        {
            containedItem.accept( po );
            po = p.pop();
        }
        if(po instanceof ArrayRule ||
           po instanceof PrimitiveRule ||
           po instanceof ObjectRule ||
           po instanceof GroupRule ||
           po instanceof TargetRuleName )
        {
            containedItem.accept( po );
        }
        else
        {
            throw new InternalError(
                "array item cannot be instace of %s", po );
        }
        if( p.peek() instanceof Combiner )
        {
            containedItem.accept( p.pop() );
        }
        p.peek().accept( containedItem );
    };

    static BiConsumer<ArrayList<Token>, JcrParser> objectConsumer = (t,p) ->
    {
        p.logTokens( t, Level.FINE, "new objectRule" );
        ObjectRule objectRule = new ObjectRule();
        consumeNonContainer(t, p, objectRule);
    };

    static BiConsumer<ArrayList<Token>, JcrParser> objectItemConsumer = (t,p) ->
    {
        p.logTokens( t, Level.FINE, "new object item" );
        ContainedItem containedItem = new ContainedItem();
        ParsedObject po = p.pop();
        if( po instanceof Repetition )
        {
            containedItem.accept( po );
            po = p.pop();
        }
        if(po instanceof MemberRule ||
           po instanceof GroupRule ||
           po instanceof TargetRuleName )
        {
            containedItem.accept( po );
        }
        else
        {
            throw new InternalError( "object item cannot be instance of %s", po );
        }
        if( p.peek() instanceof Combiner )
        {
            containedItem.accept( p.pop() );
        }
        p.peek().accept( containedItem );
    };

    static BiConsumer<ArrayList<Token>, JcrParser> groupConsumer = (t,p) ->
    {
        p.logTokens( t, Level.FINE, "new groupRule" );
        GroupRule groupRule = new GroupRule();
        consumeNonContainer(t, p, groupRule);
    };

    static BiConsumer<ArrayList<Token>, JcrParser> groupItemConsumer = (t,p) ->
    {
        p.logTokens( t, Level.FINE, "new group item" );
        ContainedItem containedItem = new ContainedItem();
        ParsedObject po = p.pop();
        if( po instanceof Repetition )
        {
            containedItem.accept( po );
            po = p.pop();
        }
        if(po instanceof MemberRule ||
           po instanceof TargetRuleName ||
           po instanceof PrimitiveRule ||
           po instanceof ObjectRule ||
           po instanceof ArrayRule ||
           po instanceof GroupRule )
        {
            containedItem.accept( po );
        }
        else
        {
            throw new InternalError( "group item cannot be instance of %s", po );
        }
        if( p.peek() instanceof Combiner )
        {
            containedItem.accept( p.pop() );
        }
        p.peek().accept( containedItem );
    };

    static BiConsumer<ArrayList<Token>, JcrParser> memberConsumer= (t,p) ->
    {
        p.logTokens( t, Level.FINE, "new memberRule" );
        MemberRule memberRule = new MemberRule(t );
        consumeNonContainer(t, p, memberRule);
    };

    static BiConsumer<ArrayList<Token>, JcrParser> combinerConsumer = (t,p) ->
    {
        Combiner combiner = Combiner.combiner( t );
        p.logTokens( t, Level.FINE, "new combiner" );
        p.push( combiner );
    };

    static BiConsumer<ArrayList<Token>, JcrParser> primitiveConsumer = (t,p) ->
    {
        PrimitiveRule primitiveRule = new PrimitiveRule(t );
        consumeNonContainer(t, p, primitiveRule);
    };

    static BiConsumer<ArrayList<Token>, JcrParser> annotationConsumer = (t,p) ->
    {
        p.logTokens( t, Level.FINE, "new annotation" );
        p.push( annotation( t.get( 0 ) ) );
    };

    static BiConsumer<ArrayList<Token>, JcrParser> repetitionConsumer = (t,p) ->
    {
        p.logTokens( t, Level.FINE, "new repetition" );
        Repetition repetition = new Repetition( t );
        p.push( repetition );
    };

    static BiConsumer<ArrayList<Token>, JcrParser> targetRuleNameConsumer = (t,p) ->
    {
        TargetRuleName targetRuleName = new TargetRuleName( t );
        consumeNonContainer( t, p, targetRuleName );
    };

    static BiConsumer<ArrayList<Token>, JcrParser> namedRuleConsumer = (t,p) ->
    {
        NamedRule namedRule = new NamedRule( t );
        consumeNonContainer( t, p, namedRule );
    };

    static BiConsumer<ArrayList<Token>, JcrParser> jcrVersionDirectiveConsumer = (t,p) ->
    {
        p.logTokens( t, Level.FINE, "new jcr-version directive" );
        JcrVersionDirective d = new JcrVersionDirective( t );
        p.push( d );
    };

    static BiConsumer<ArrayList<Token>, JcrParser> rulesetIdDirectiveConsumer = (t,p) ->
    {
        p.logTokens( t, Level.FINE, "new ruleset-id directive" );
        RuleSetIdDirective d = new RuleSetIdDirective(t );
        p.push( d );
    };

    static BiConsumer<ArrayList<Token>, JcrParser> importDirectiveConsumer = (t,p) ->
    {
        p.logTokens( t, Level.FINE, "new import directive" );
        ImportDirective d = new ImportDirective( t );
        p.push( d );
    };

    static void consumeNonContainer( ArrayList<Token> t, JcrParser p, ParsedObject o )
    {
        //get any annotations
        while( p.peek() instanceof Annotation )
        {
            //remove the annotation and apply it to the ParsedObject
            Annotation a = (Annotation) p.pop();
            o.accept( a );
        }
        //log it
        p.logTokens( t, Level.FINE, "new " + o );
        //push it on the stack
        p.push( o );
    }
}
