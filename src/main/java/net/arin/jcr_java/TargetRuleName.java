/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import java.util.ArrayList;
import java.util.logging.Logger;

import static net.arin.jcr_java.Annotation.NOT;

/**
 * Represents a JCR target rule name.
 */
public class TargetRuleName implements ParsedObject
{
    Logger log = Logger.getLogger( getClass().getName() );

    Annotation not = null;

    String targetRuleName = null;
    String ruleSetAlias = null;

    public TargetRuleName( ArrayList<Token> tokens )
    {
        //just a simple name
        if( tokens.size() == 2 && tokens.get( 1 ).tokenType == TokenType.NAME )
        {
            targetRuleName = tokens.get( 1 ).sequence;
        }
        else if( tokens.size() == 4 &&
            tokens.get( 1 ).tokenType == TokenType.NAME &&
            tokens.get( 2 ).tokenType == TokenType.PERIOD &&
            tokens.get( 3 ).tokenType == TokenType.NAME )
        {
            ruleSetAlias = tokens.get( 1 ).sequence;
            targetRuleName = tokens.get( 3 ).sequence;
        }
    }

    @Override
    public String toString()
    {
        if( ruleSetAlias != null )
        {
            return String.format( "%s$%s.%s", Annotation.toString( not ), ruleSetAlias, targetRuleName );
        }
        //else
        return String.format( "%s$%s", Annotation.toString( not ), targetRuleName );
    }

    @Override
    public void accept( ParsedObject parsedObject )
    {
        if( parsedObject instanceof Annotation )
        {
            Annotation a = (Annotation) parsedObject;
            if( a == NOT )
            {
                not = a;
            }
            else
            {
                log.warning( "unknown annotation applied to target rule name: " + a.getToken() );
            }
        }
        else
        {
            throw new InternalError( "target rule name cannot accept: ", parsedObject );
        }
    }
}
