/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import java.util.logging.Logger;

/**
 * Represents an item that is contained in an object, array or group usually
 * denoted by the array_item, object_item, or group_item in the ABNF. This object
 * is a holder for real JCR rules, but has the combiner and repetition constructs
 * that are only valid in the context of being inside an array, group, or object.
 */
public class ContainedItem implements ParsedObject
{
    Logger log = Logger.getLogger(getClass().getName() );

    Combiner combiner;
    ParsedObject jsonContentRule;
    Repetition repetition = new Repetition();

    @Override
    public void accept( ParsedObject parsedObject )
    {
        log.finer( "Accepting parsed object " + parsedObject + " to contained item" );
        if( parsedObject instanceof Combiner )
        {
            combiner = (Combiner) parsedObject;
        }
        else if( parsedObject instanceof  Repetition )
        {
            repetition = (Repetition) parsedObject;
        }
        else if(parsedObject instanceof MemberRule ||
                parsedObject instanceof TargetRuleName ||
                parsedObject instanceof PrimitiveRule ||
                parsedObject instanceof ArrayRule ||
                parsedObject instanceof ObjectRule ||
                parsedObject instanceof GroupRule)
        {
            jsonContentRule = parsedObject;
        }
        else
        {
            throw new InternalError( "Parsed object %s cannot be added an contained item.", parsedObject );
        }
    }

    @Override
    public String toString()
    {
        if( combiner != null )
        {
            return combiner.toString() + " " + jsonContentRule.toString() + repetition.toString();
        }
        //else
        return jsonContentRule.toString() + repetition.toString();
    }

    @Override
    public ParsedObject getSub( int index )
    {
        if( index == 0 )
        {
            return jsonContentRule;
        }
        //else
        return null;
    }

}
