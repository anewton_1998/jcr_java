/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (c) 2013 Cogito Learning Ltd
 * Copyright (c) 2016-2017 ARIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package net.arin.jcr_java;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * A class for reading an input string and separating it into tokens that can be
 * fed into Parser. This tokenizer does not ignore whitespace.
 */
public class Tokenizer
{

    /**
     * a list of TokenInfo objects
     *
     * Each token type corresponds to one entry in the list
     */
    private LinkedList<TokenExtractor> tokenExtractors;

    /** the list of tokens produced when tokenizing the input */
    private ArrayList<Token> tokens;

    /** a tokenizer that can handle JCR */
    private static Tokenizer tokenizer = null;

    /**
     * Default constructor
     */
    public Tokenizer()
    {
        super();
        tokenExtractors = new LinkedList<>();
        tokens = new ArrayList<>();
    }

    /**
     * A static method that returns a tokenizer
     * @return a tokenizer that can handle JCR
     */
    public static Tokenizer getJCRTokenizer()
    {
        if ( tokenizer == null)
            tokenizer = createJCRTokenizer();
        return tokenizer;
    }

    /**
     * A static method that actually creates a tokenizer for JCR
     * @return a tokenizer that can handle JCR
     */
    private static Tokenizer createJCRTokenizer()
    {
        Tokenizer tokenizer = new Tokenizer();

        for ( TokenType tokenType : TokenType.values() )
        {
            tokenizer.add( tokenType );
        }

        return tokenizer;
    }

    public void add( TokenType tokenType )
    {
        tokenExtractors.add( tokenType.getTokenExtractor() );
    }

    /**
     * Tokenize an array of strings.
     * @param strings an array of strings
     */
    public void tokenize( String[] strings )
    {
        int i = 0;
        for ( String string : strings )
        {
            tokenize( string, i );
            i++;
        }
    }

    /**
     * Tokenize the input str.
     * @param str the string to tokenize
     */
    public void tokenize( String str )
    {
        String[] strings = str.split( "\\n" );
        tokenize( strings );
    }

    /**
     * Tokenize an input string.
     *
     * The result of tokenizing can be accessed via getTokens
     *
     * @param str the string to tokenize
     * @param line line number
     */
    public void tokenize(String str, int line )
    {
        String s = str;
        int totalLength = s.length();
        if( line == 0 )
        {
            tokens.clear();
        }
        while (!s.equals(""))
        {
            int remaining = s.length();
            boolean match = false;
            for (TokenExtractor info : tokenExtractors )
            {
                String tok = info.extract( s );
                if( tok != null )
                {
                    match = true;
                    s = s.substring( tok.length() );
                    tokens.add( new Token( info.getTokenType(), tok, totalLength - remaining + 1, line ) );
                    break;
                }
            }
            if (!match)
                throw new ParsingException("Unexpected character while tokenizing with input: " + s);
        }
    }

    /**
     * Get the tokens generated in the last call to tokenize.
     * @return a list of tokens to be fed to Parser
     */
    public ArrayList<Token> getTokens()
    {
        return tokens;
    }

}