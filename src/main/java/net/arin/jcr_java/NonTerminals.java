/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import net.arin.jcr_java.JcrParser.Repeat;

import java.util.logging.Logger;

import static net.arin.jcr_java.JcrParser.pChoice;
import static net.arin.jcr_java.JcrParser.pConsume;
import static net.arin.jcr_java.JcrParser.pMaybe;
import static net.arin.jcr_java.JcrParser.pSeq;
import static net.arin.jcr_java.NonTerminal.ntRule;
import static net.arin.jcr_java.NonTerminal.parsingRuleMaybe;

/**
 * Contains various non-terminals
 */
class NonTerminals
{
    private static final String JCR                   = "jcr";
    private static final String SPC_CMNT              = "spcCmnt";
    private static final String ROOT_RULE             = "root_rule";
    private static final String RULE                  = "rule";
    private static final String VALUE_RULE            = "value_rule";
    private static final String ANNOTATIONS           = "annotations";
    private static final String SPC_CMNT_Q            = "spcCmnt?";
    private static final String RULE_DEF              = "rule_def";
    private static final String ARRAY_RULE            = "array_rule";
    private static final String TARGET_RULE_NAME      = "target_rule_name";
    private static final String SPACES_Q              = "spaces?";
    private static final String PRIMITIVE_RULE        = "primitive_rule";
    private static final String MEMBER_RULE           = "member_rule";
    private static final String MEMBER_NAME_SPEC      = "member_name_spec";
    private static final String TYPE_RULE             = "type_rule";
    private static final String ARRAY_ITEM_TYPES      = "array_item_types";
    private static final String ARRAY_ITEM            = "array_item";
    private static final String ARRAY_ITEMS           = "array_items";
    private static final String REPETITION            = "repetition";
    private static final String PRIMITIVE_DEF         = "primitive_def";
    private static final String FLOAT_RANGE           = "float_range";
    private static final String INTEGER_RANGE         = "integer_range";
    private static final String INTEGER               = "integer";
    private static final String SIZED_INT_TYPE        = "sized_int_type";
    private static final String SIZED_UINT_TYPE       = "sized_uint_type";
    private static final String SEQUENCE_COMBINER     = "sequence_combiner";
    private static final String CHOICE_COMBINER       = "choice_combiner";
    private static final String ANNOTATION_SET        = "annotation_set";
    private static final String TBD_ANNOTATION        = "tbd_annotation";
    private static final String MULTI_LINE_PARAMETERS = "multi_line_parameters";
    private static final String NON_NEG_INTEGER       = "non_neg_integer";
    private static final String ONE_OR_MORE           = "one_or_more";
    private static final String REPETITION_RANGE      = "repetition_range";
    private static final String ZERO_OR_MORE          = "zero_or_more";
    private static final String MIN_MAX_REPETITION    = "min_max_repetition";
    private static final String MIN_REPETITION        = "min_repetition";
    private static final String MAX_REPETITION        = "max_repetition";
    private static final String REPETITION_STEP       = "repetition_step";
    private static final String TYPE_DESIGNATOR       = "type_designator";
    private static final String RULE_DEF_TYPE_RULE    = "rule_def_type_rule";
    private static final String OBJECT_ITEM_TYPES     = "object_item_types";
    private static final String OBJECT_ITEM           = "object_item";
    private static final String OBJECT_ITEMS          = "object_items";
    private static final String OBJECT_RULE           = "object_rule";
    private static final String GROUP_RULE            = "group_rule";
    private static final String TYPE_CHOICE_ITEMS     = "type_choice_items";
    private static final String TYPE_CHOICE           = "type_choice";
    private static final String EXPLICIT_TYPE_CHOICE  = "explicit_type_choice";
    private static final String ARRAY_GROUP           = "array_group";
    private static final String OBJECT_GROUP          = "object_group";
    private static final String DSPS                  = "DSPs";
    private static final String DIRECTIVE             = "directive";
    private static final String ONE_LINE_DIRECTIVE    = "one-line-directive";
    private static final String MULTI_LINE_DIRECTIVE  = "multi-line-directive";
    private static final String DIRECTIVE_DEF         = "directive-def";
    private static final String JCR_VERSION_D         = "jcr-version-d";
    private static final String RULESET_ID_D          = "ruleset-id-d";
    private static final String IMPORT_D              = "import-d";
    private static final String ALPHA_TOKEN           = "alpha-token";

    static Logger log = Logger.getLogger( NonTerminals.class.getName() );

    static void setupParsingRules(JcrParser p)
    {

        /*
         * jcr = *( sp-cmt / directive / root_rule / rule )
         */
        p.newNtRule(JCR,
                    pChoice(Repeat.ZERO_OR_MORE,
                            ntRule(SPC_CMNT ),
                            pConsume((t,j) -> j.popAndConsume(), ntRule( DIRECTIVE ) ),
                            pConsume((t,j) -> j.popAndConsume(), ntRule( ROOT_RULE ) ),
                            pConsume((t,j) -> j.popAndConsume(), ntRule( RULE ) )
            )
        );

        /*
         * root_rule = value_rule / group_rule
         */
        p.newNtRule(ROOT_RULE,
                    pChoice(Repeat.ONCE,
                            ntRule(VALUE_RULE ),
                            ntRule(GROUP_RULE )
            )
        );

        /*
         *  rule = annotations "$" rule_name spcCmnt?
         *        "=" spcCmnt? rule_def
         *  rule_name = name
         */
        p.newNtRule(RULE,
                    pSeq(ntRule(ANNOTATIONS),
                         TokenType.DOLLAR,
                         pConsume(NTConsumers.namedRuleConsumer, TokenType.NAME),
                         ntRule(SPC_CMNT_Q),
                         TokenType.EQUALS_SIGN,
                         ntRule(SPC_CMNT_Q),
                         pConsume( (t, j) -> j.popAndConsume(), ntRule( RULE_DEF) )
                    )
        );

        /*
         * rule_def = member_rule / type_designator rule_def_type_rule /
         *            array_rule / object_rule / group_rule /
         *            target_rule_name
         */
        p.newNtRule(RULE_DEF,
                    pChoice(ntRule(MEMBER_RULE),
                            pSeq(ntRule(TYPE_DESIGNATOR),
                                 ntRule(RULE_DEF_TYPE_RULE)
                            ),
                            ntRule(ARRAY_RULE),
                            ntRule(OBJECT_RULE),
                            ntRule(GROUP_RULE),
                            ntRule(TARGET_RULE_NAME)
                    )
        );

        /*
         * type_designator = type-kw 1*spcCmnt / ":" spcCmnt?
         */
        p.newNtRule(TYPE_DESIGNATOR,
                    pChoice(
                            pSeq(TokenType.TYPE_KW,
                                 ntRule(SPC_CMNT)
                            ),
                            pSeq(TokenType.COLLON,
                                 ntRule(SPC_CMNT_Q)
                            )
                    )
        );

        /*
         * rule_def_type_rule = value_rule / type_choice
         */
       p.newNtRule(RULE_DEF_TYPE_RULE,
                   pChoice(
                           ntRule(VALUE_RULE ),
                           ntRule(TYPE_CHOICE)
           )
       );

        /*
         * spaces? -> [ spaces ]
         */
        p.newNtRule(SPACES_Q,
                    pChoice(
                            Repeat.OPTIONAL,
                            TokenType.SPACES
                    )
        );

        /*
         * spcCmnt = spaces / comment
         */
        p.newNtRule(SPC_CMNT,
                    pChoice(Repeat.ONCE,
                            TokenType.SPACES,
                            TokenType.COMMENT
                    )
        );

        /*
         * spcCmnt? -> *sp-cmt
         */
        p.newNtRule(SPC_CMNT_Q,
                    pSeq(Repeat.ZERO_OR_MORE, ntRule(SPC_CMNT ) ) );

        /*
         * DSPs             = ; Directive spaces
         *                    1*WSP /     ; When in one-line directive
         *                    1*sp-cmt   ; When in muti-line directive
         */
        p.newNtRule( DSPS,
                     pChoice(
                             pSeq( Repeat.ONE_OR_MORE,
                                   TokenType.SPACES
                             ),
                             pSeq( Repeat.ONE_OR_MORE,
                                   ntRule( SPC_CMNT )
                             )
                     )
        );

        /*
         * directive        = "#" (one-line-directive / multi-line-directive)
         */
        p.newNtRule( DIRECTIVE,
                     pSeq(
                             TokenType.OCTOTHORPE,
                             pChoice(
                                     ntRule(MULTI_LINE_DIRECTIVE),
                                     ntRule(ONE_LINE_DIRECTIVE)
                             )
                     )
        );

        /*
         * one-line-directive = [ DSPs ]
         *                      (directive-def / one-line-tbd-directive-d)
         *                      *WSP eol
         *
         * //todo not supporting unknown directives just yet
         */
        p.newNtRule( ONE_LINE_DIRECTIVE,
                     pSeq(
                             pMaybe( ntRule( DSPS ) ),
                             ntRule(DIRECTIVE_DEF)

                     )
        );

        /*
         * multi-line-directive = "{" *sp-cmt
         *                        ( directive-def /
         *                          multi-line-tbd-directive-d )
         *                          *sp-cmt "}"
         *
         * //todo not supporting unknown directives just yet
         */
        p.newNtRule( MULTI_LINE_DIRECTIVE,
                     pSeq(
                             TokenType.LEFT_CURLY,
                             ntRule( SPC_CMNT_Q ),
                             ntRule( DIRECTIVE_DEF ),
                             ntRule( SPC_CMNT_Q ),
                             TokenType.RIGHT_CURLY
                     )
        );

        /*
         * directive-def    = jcr-VERSION-d / ruleset-id-d / import-d
         */
        p.newNtRule( DIRECTIVE_DEF,
                     pChoice(
                             pConsume( NTConsumers.jcrVersionDirectiveConsumer, ntRule(JCR_VERSION_D ) ),
                             pConsume( NTConsumers.rulesetIdDirectiveConsumer, ntRule(RULESET_ID_D) ),
                             pConsume( NTConsumers.importDirectiveConsumer, ntRule(IMPORT_D) )
                     )
        );

        /*
         * jcr-VERSION-d    = jcr-VERSION-kw DSPs major-VERSION
         *                    "." minor-VERSION
         *                    *( DSPs "+" [ DSPs ] extension-id )
         *
         * major-VERSION    = non-neg-integer
         *
         * minor-VERSION    = non-neg-integer
         *
         * extension-id     = ALPHA *not-space
         *
         * //todo come back changed FLOAT to major "." minor
         */
        p.newNtRule( JCR_VERSION_D,
                     pSeq(
                             TokenType.JCR_VERSION_KW,
                             ntRule( DSPS ),
                             TokenType.FLOAT,
                             pSeq( Repeat.ZERO_OR_MORE,
                                   ntRule( DSPS ),
                                   TokenType.PLUS,
                                   pMaybe( ntRule( DSPS ) ),
                                   ntRule(ALPHA_TOKEN)
                             )
                     )
        );

        /*
         * alpha-token is used by ruleset-id and extension-id.
         * however, it is problematic since the tokenizer will match other reserved words with it.
         * this is tricky because the tokenizer actually has multiple tokens that will match this
         * Must be a combination of a bunch of things
         * any-kw
         * as-kw
         * base32-kw
         * base32hex-kw
         * base64-kw
         * base64url-kw
         * boolean-kw
         * date-kw
         * datetime-kw
         * double-kw
         * email-kw
         * false-kw
         * float-kw
         * fqdn-kw
         * hex-kw
         * idn-kw
         * import-kw
         * int-kw
         * integer-kw
         * ipaddr-kw
         * ipv4-kw
         * ipv6-kw
         * jcr-VERSION-kw
         * not-kw
         * null-kw
         * phone-kw
         * root-kw
         * ruleset-id-kw
         * string-kw
         * time-kw
         * true-kw
         * type-kw
         * uint-kw
         * unordered-kw
         * uri-kw
         */
        p.newNtRule( ALPHA_TOKEN,
                     pChoice(
                             TokenType.ALPHATOKEN,
                             TokenType.NAME,
                             TokenType.ANY_KW,
                             TokenType.AS_KW,
                             TokenType.BASE32_KW,
                             TokenType.BASE32HEX_KW,
                             TokenType.BASE64_KW,
                             TokenType.BASE64URL_KW,
                             TokenType.BOOLEAN_KW,
                             TokenType.DATE_KW,
                             TokenType.DATETIME_KW,
                             TokenType.DOUBLE_KW,
                             TokenType.EMAIL_KW,
                             TokenType.FALSE_KW,
                             TokenType.FLOAT_KW,
                             TokenType.HEX_KW,
                             TokenType.IDN_KW,
                             TokenType.IMPORT_KW,
                             TokenType.INT_KW,
                             TokenType.INTEGER_KW,
                             TokenType.IPADDR_KW,
                             TokenType.IPV4_KW,
                             TokenType.IPV6_KW,
                             TokenType.JCR_VERSION_KW,
                             TokenType.NOT_KW,
                             TokenType.NULL_KW,
                             TokenType.PHONE_KW,
                             TokenType.ROOT_KW,
                             TokenType.RULESET_ID_KW,
                             TokenType.STRING_KW,
                             TokenType.TIME_KW,
                             TokenType.TRUE_KW,
                             TokenType.TYPE_KW,
                             TokenType.UINT_KW,
                             TokenType.UNORDERED_KW,
                             TokenType.URI_KW
                     )
        );

        /*
         * ruleset-id-d     = ruleset-id-kw DSPs ruleset-id
         *
         * ruleset-id       = ALPHA *not-space
         */
        p.newNtRule( RULESET_ID_D,
                     pSeq(
                             TokenType.RULESET_ID_KW,
                             ntRule( DSPS ),
                             ntRule( ALPHA_TOKEN )
                     )
        );

        /*
         * import-d         = import-kw DSPs ruleset-id
         *                    [ DSPs as-kw DSPs ruleset-id-alias ]
         *
         * ruleset-id-alias = name
         */
        p.newNtRule( IMPORT_D,
                     pSeq(
                             TokenType.IMPORT_KW,
                             ntRule( DSPS ),
                             ntRule( ALPHA_TOKEN ),
                             pMaybe(
                                     ntRule( DSPS ),
                                     TokenType.AS_KW,
                                     ntRule( DSPS ),
                                     TokenType.NAME
                             )
                     )
        );

        /*
         * value_rule = primitive_rule / array_rule / object_rule
         */
        p.newNtRule(VALUE_RULE, pChoice(Repeat.ONCE,
                                        ntRule(PRIMITIVE_RULE ),
                                        ntRule(ARRAY_RULE ),
                                        ntRule(OBJECT_RULE )
            )
        );

        /*
         * member_rule = annotations
                         member_name_spec spcCmnt? ":" spcCmnt? type_rule
         */
        p.newNtRule(MEMBER_RULE,
                    pSeq(
                            ntRule(ANNOTATIONS),
                            pConsume(NTConsumers.memberConsumer, ntRule(MEMBER_NAME_SPEC) ),
                            ntRule(SPC_CMNT_Q),
                            TokenType.COLLON,
                            ntRule(SPC_CMNT_Q),
                            pConsume((t, j) -> j.popAndConsume(), ntRule(TYPE_RULE))
                    )
        );

        /*
         * member_name_spec = regex / q_string
         */
        p.newNtRule(MEMBER_NAME_SPEC,
                    pChoice( TokenType.REGEX, TokenType.QSTRING ));

        /*
         * type_rule = value_rule / type_choice / target_rule_name
         */
        p.newNtRule(TYPE_RULE, pChoice(Repeat.ONCE,
                                       ntRule(VALUE_RULE),
                                       ntRule(TYPE_CHOICE),
                                       ntRule(TARGET_RULE_NAME)
                    )
        );

        /*
         * type_choice = annotations "(" type_choice_items
         *               *( choice_combiner type_choice_items ) ")"
         */
        p.newNtRule( TYPE_CHOICE ,
                     pSeq(
                           ntRule( ANNOTATIONS ),
                           pConsume( NTConsumers.groupConsumer, TokenType.LEFT_PARENTHESIS ),
                           pConsume( NTConsumers.groupItemConsumer, ntRule( TYPE_CHOICE_ITEMS ) ),
                           pSeq( Repeat.ZERO_OR_MORE,
                                 pConsume( NTConsumers.combinerConsumer, ntRule( CHOICE_COMBINER ) ),
                                 pConsume( NTConsumers.groupItemConsumer, ntRule( TYPE_CHOICE_ITEMS ) )
                           ),
                           TokenType.RIGHT_PARENTHESIS
                     )
        );

        /*
         * explicit_type_choice = type_designator type_choice
         */
        p.newNtRule(EXPLICIT_TYPE_CHOICE,
                    pSeq(
                         ntRule( TYPE_DESIGNATOR ),
                         ntRule( TYPE_CHOICE )
                     )
        );

        /*
         * type_choice_items = spcCmnt? ( type_choice / type_rule ) spcCmnt?
         */
        p.newNtRule(TYPE_CHOICE_ITEMS,
                    pSeq(
                          ntRule( SPC_CMNT_Q ),
                          pChoice(
                                  ntRule( TYPE_CHOICE ),
                                  ntRule( TYPE_RULE )
                          ),
                          ntRule( SPC_CMNT_Q )
                    )
        );

        /*
         * target_rule_name  = annotations "$"
         *                     [ ruleset_id_alias "." ]
         *                     rule_name
         * ruleset_id_alias = name
         * rule_name = name
         */
        p.newNtRule(TARGET_RULE_NAME,
                    pSeq(
                          ntRule(ANNOTATIONS),
                          pConsume(NTConsumers.targetRuleNameConsumer,
                                   pSeq(
                                        TokenType.DOLLAR,
                                        pMaybe(
                                               TokenType.NAME,
                                               TokenType.PERIOD
                                        ),
                                        TokenType.NAME
                                   )
                          )
                    )
        );

        /*
        * object_rule = annotations "{" spcCmnt?
        *                               [ object_items spcCmnt? ] "}"
        */
        p.newNtRule(OBJECT_RULE,
                    pSeq(Repeat.ONCE,
                         ntRule(ANNOTATIONS),
                         pConsume(NTConsumers.objectConsumer, TokenType.LEFT_CURLY),
                         ntRule(SPC_CMNT_Q),
                         pMaybe(
                                 ntRule(OBJECT_ITEMS),
                                 ntRule(SPC_CMNT_Q)
                         ),
                         TokenType.RIGHT_CURLY
                    )
        );

        /*
         * object-items     = object-item [ 1*( sequence-combiner object-item ) /
         *                                  1*( choice-combiner object-item ) ]
         */
        p.newNtRule( OBJECT_ITEMS,
                     pSeq( Repeat.ONCE,
                           ntRule( OBJECT_ITEM ),
                           pMaybe(
                                   pChoice( Repeat.ONCE,
                                            pSeq(Repeat.ONE_OR_MORE,
                                                 pConsume(NTConsumers.combinerConsumer, ntRule(SEQUENCE_COMBINER)),
                                                 ntRule(OBJECT_ITEM)
                                            ),
                                            pSeq(Repeat.ONE_OR_MORE,
                                                 pConsume(NTConsumers.combinerConsumer, ntRule(CHOICE_COMBINER)),
                                                 ntRule(OBJECT_ITEM)
                                            )
                                   )
                           )
                     )
        );

        /*
         * object_item = object_item_types spcCmnt? [ repetition ]
         */
        p.newNtRule(OBJECT_ITEM,
                    pConsume(
                            NTConsumers.objectItemConsumer,
                            pSeq(Repeat.ONCE,
                                 ntRule(OBJECT_ITEM_TYPES),
                                 ntRule(SPC_CMNT_Q),
                                 parsingRuleMaybe(REPETITION)
                            )
                    )
        );

        /*
         * object_item_types = object_group / member_rule / target_rule_name
         */
        p.newNtRule(OBJECT_ITEM_TYPES,
                    pChoice(Repeat.ONCE,
                            ntRule(OBJECT_GROUP),
                            ntRule(MEMBER_RULE),
                            ntRule(TARGET_RULE_NAME)
                    )
        );

        /*
         * object_group = annotations "(" spcCmnt? [ object_items spcCmnt? ] ")"
         */
        p.newNtRule( OBJECT_GROUP,
                     pSeq( Repeat.ONCE,
                           ntRule( ANNOTATIONS ),
                           pConsume( NTConsumers.groupConsumer, TokenType.LEFT_PARENTHESIS ),
                           ntRule( SPC_CMNT_Q ),
                           pMaybe(
                                   ntRule( OBJECT_ITEMS ),
                                   ntRule( SPC_CMNT_Q )
                           ),
                           TokenType.RIGHT_PARENTHESIS
                     )
        );

        /*
         * array_item_types = array_group / type_rule / explicit_type_choice
         */
        p.newNtRule(ARRAY_ITEM_TYPES,
                    pChoice(Repeat.ONCE,
                            ntRule( ARRAY_GROUP ),
                            ntRule(TYPE_RULE),
                            ntRule(EXPLICIT_TYPE_CHOICE)
                    )
        );

        /*
         * array_group = annotations "(" spcCmnt? [ array_items spcCmnt? ] ")"
         */
        p.newNtRule( ARRAY_GROUP,
                     pSeq( Repeat.ONCE,
                           ntRule( ANNOTATIONS ),
                           pConsume( NTConsumers.groupConsumer, TokenType.LEFT_PARENTHESIS ),
                           ntRule( SPC_CMNT_Q ),
                           pMaybe(
                                   ntRule( ARRAY_ITEMS ),
                                   ntRule( SPC_CMNT_Q )
                           ),
                           TokenType.RIGHT_PARENTHESIS
                     )
        );

        /*
         * array_item = array_item_types spcCmnt? [ repetition ]
         */
        p.newNtRule(ARRAY_ITEM,
                    pConsume(NTConsumers.arrayItemConsumer,
                             pSeq(Repeat.ONCE,
                                  ntRule(ARRAY_ITEM_TYPES),
                                  ntRule(SPC_CMNT_Q),
                                  parsingRuleMaybe(REPETITION)
                             )
                    )
        );

        /*
         * array-items      = array-item [ 1*( sequence-combiner array-item ) /
         *                                 1*( choice-combiner array-item ) ]
         */
        p.newNtRule( ARRAY_ITEMS,
                     pSeq( Repeat.ONCE,
                           ntRule( ARRAY_ITEM ),
                           pMaybe(
                                  pChoice(
                                          pSeq( Repeat.ONE_OR_MORE,
                                                pConsume(NTConsumers.combinerConsumer, ntRule(SEQUENCE_COMBINER)),
                                                ntRule(ARRAY_ITEM)
                                          ),
                                          pSeq( Repeat.ONE_OR_MORE,
                                                pConsume(NTConsumers.combinerConsumer, ntRule(CHOICE_COMBINER)),
                                                ntRule(ARRAY_ITEM)
                                          )
                                  )
                           )
                     )
        );

        /*
         * array_rule = annotations "[" spcCmnt? [ array_items spcCmnt? ] "]"
         */
        p.newNtRule(ARRAY_RULE,
                    pSeq(Repeat.ONCE,
                         ntRule(ANNOTATIONS),
                         pConsume(NTConsumers.newArrayConsumer, TokenType.LEFT_BRACKET),
                         ntRule(SPC_CMNT_Q),
                         pMaybe(
                                 ntRule(ARRAY_ITEMS),
                                 ntRule(SPC_CMNT_Q)
                         ),
                         TokenType.RIGHT_BRACKET
                    )
        );


        /*
         * group-rule       = annotations "(" *sp-cmt
         *                    ( [object-items] ")" / [array-items] ")" )
         *
         *                    *note, [object-items] and [array-items] is not optional in our code below
         */
        p.newNtRule( GROUP_RULE,
                     pSeq( Repeat.ONCE,
                           ntRule( ANNOTATIONS ),
                           pConsume( NTConsumers.groupConsumer, TokenType.LEFT_PARENTHESIS ),
                           ntRule( SPC_CMNT_Q ),
                           pChoice( Repeat.ONCE,
                                    pSeq( ntRule( OBJECT_ITEMS ),
                                          TokenType.RIGHT_PARENTHESIS
                                    ),
                                    pSeq( ntRule( ARRAY_ITEMS ),
                                          TokenType.RIGHT_PARENTHESIS
                                    )
                           )
                     )
        );

        /*
         * primitive_rule = annotations primitive_def
         */
        p.newNtRule(PRIMITIVE_RULE,
                    pSeq(Repeat.ONCE,
                         ntRule(ANNOTATIONS),
                         ntRule(PRIMITIVE_DEF)
                    )
        );

        /*
         * primitive_def = string_type / string_range / string_value /
         *             null_type / boolean_type / true_value / false_value /
         *             double_type / float_type / float_range / float_value /
         *             integer_type / integer_range / integer_value /
         *             sized_int_type / sized_uint_type /
         *             ipv4_type / ipv6_type / ipaddr_type / fqdn_type / idn_type /
         *             uri_range / uri_type / phone_type / email_type /
         *             datetime_type / date_type / time_type /
         *             hex_type / base32hex_type / base32_type / base64url_type / base64_type /
         *             any
         */
        p.newNtRule(PRIMITIVE_DEF,
                    pConsume(NTConsumers.primitiveConsumer,
                             pChoice(Repeat.ONCE,
                                     TokenType.STRING_KW,  //string_type
                                     TokenType.REGEX,      //string_range
                                     TokenType.QSTRING,    //string_value
                                     TokenType.NULL_KW,
                                     TokenType.BOOLEAN_KW,
                                     TokenType.TRUE_KW,
                                     TokenType.FALSE_KW,
                                     TokenType.DOUBLE_KW,
                                     TokenType.FLOAT_KW,
                                     ntRule(FLOAT_RANGE),
                                     TokenType.FLOAT,
                                     TokenType.INTEGER_KW,
                                     ntRule(INTEGER_RANGE),
                                     ntRule(INTEGER),
                                     ntRule(SIZED_INT_TYPE),
                                     ntRule(SIZED_UINT_TYPE),
                                     TokenType.IPV4_KW,
                                     TokenType.IPV6_KW,
                                     TokenType.IPADDR_KW,
                                     TokenType.FQDN_KW,
                                     TokenType.IDN_KW,
                                     TokenType.URI_RANGE,
                                     TokenType.URI_KW,
                                     TokenType.PHONE_KW,
                                     TokenType.EMAIL_KW,
                                     TokenType.DATETIME_KW,
                                     TokenType.DATE_KW,
                                     TokenType.TIME_KW,
                                     TokenType.HEX_KW,
                                     TokenType.BASE32HEX_KW,
                                     TokenType.BASE32_KW,
                                     TokenType.BASE64URL_KW,
                                     TokenType.BASE64_KW,
                                     TokenType.ANY_KW
                             )
                    )
        );

        /*
         * sequence_combiner = spcCmnt? "," spcCmnt?
         */
        p.newNtRule(SEQUENCE_COMBINER,
                    pSeq(Repeat.ONCE,
                         ntRule(SPC_CMNT_Q),
                         TokenType.COMMA,
                         ntRule(SPC_CMNT_Q)
                    )
        );

        /*
         * choice_combiner = spcCmnt? "|" spcCmnt?
         */
        p.newNtRule(CHOICE_COMBINER,
                    pSeq(Repeat.ONCE,
                         ntRule(SPC_CMNT_Q),
                         TokenType.PIPE,
                         ntRule(SPC_CMNT_Q)
                    )
        );


        /*
         *
         * annotations = *( "@{" spcCmnt? annotation_set spcCmnt? "}" spcCmnt? )
         */
        p.newNtRule(ANNOTATIONS,
                    pSeq(Repeat.ZERO_OR_MORE,
                         TokenType.AT_SIGN,
                         TokenType.LEFT_CURLY,
                         ntRule(SPC_CMNT_Q),
                         ntRule(ANNOTATION_SET),
                         ntRule(SPC_CMNT_Q),
                         TokenType.RIGHT_CURLY,
                         ntRule(SPC_CMNT_Q)
                    )
        );

        /*
         *
         * annotation_set = not_annotation / unordered_annotation /
         *                  root_annotation / tbd_annotation
         */
        p.newNtRule(ANNOTATION_SET,
                    pChoice(Repeat.ONCE,
                            pConsume(NTConsumers.annotationConsumer, TokenType.NOT_KW),
                            pConsume(NTConsumers.annotationConsumer, TokenType.UNORDERED_KW),
                            pConsume(NTConsumers.annotationConsumer, TokenType.ROOT_KW),
                            ntRule(TBD_ANNOTATION)
                    )
        );

        /*
         * We directly translate annotation_name to name and annotation_parameters to multi_line_parameters
         *
         * tbd_annotation = annotation_name [ spaces annotation_parameters ]
         * annotation_name = name
         * annotation_parameters = multi_line_parameters
         */
        p.newNtRule(TBD_ANNOTATION,
                    pSeq(Repeat.ONCE,
                         TokenType.NAME,
                         pChoice(Repeat.ONCE,
                                 TokenType.SPACES,
                                 ntRule(MULTI_LINE_PARAMETERS)
                         )
                    )
        );

        /*
         * We don't have to worry about not_multi_line_special because the tokenizer
         * abstracts that away.
         *
         * multi_line_parameters = *(comment / q_string / regex /
         *                         not_multi_line_special)
         * not_multi_line_special = spaces / %x21 / %x23-2E / %x30-3A / %x3C-7C /
         *                          %x7E-10FFFF ; not ", /, ; or }
         */
        p.newNtRule(MULTI_LINE_PARAMETERS,
                    pChoice(Repeat.ZERO_OR_MORE,
                            TokenType.COMMENT,
                            TokenType.QSTRING,
                            TokenType.REGEX
                    )
        );

        /*
         * non_neg_integer = "0" / pos_integer
         */
        p.newNtRule(NON_NEG_INTEGER,
                    pChoice(Repeat.ONCE,
                            TokenType.ZERO,
                            TokenType.POS_INTEGER
                    )
        );

        /*
         * integer = "0" / ["-"] pos_integer
         */
        p.newNtRule(INTEGER,
                    pChoice(Repeat.ONCE,
                            TokenType.ZERO,
                            pSeq(
                                 pMaybe(TokenType.MINUS),
                                 TokenType.POS_INTEGER
                            )
                    )
        );

        /*
         * integer_range = integer_min ".." [ integer_max ] / ".." integer_max
         * integer_min = integer
         * integer_max = integer
         */
        p.newNtRule(INTEGER_RANGE,
                    pChoice(
                            pSeq(
                                 ntRule(INTEGER),
                                 TokenType.DOUBLEDOT,
                                 pMaybe(ntRule(INTEGER))
                            ),
                            pSeq(
                                 TokenType.DOUBLEDOT,
                                 ntRule(INTEGER)
                            )
                    )
        );

        /*
         * sized_int_type = int-kw pos_integer
         */
        p.newNtRule(SIZED_INT_TYPE,
                    pSeq(
                         TokenType.INT_KW,
                         TokenType.POS_INTEGER
                    )
        );

        /*
         * sized_uint_type = uint-kw pos_integer
         */
        p.newNtRule(SIZED_UINT_TYPE,
                    pSeq(
                         TokenType.UINT_KW,
                         TokenType.POS_INTEGER
                    )
        );

        /*
         * float_range = float_min ".." [ float_max ] / ".." float_max
         */
        p.newNtRule(FLOAT_RANGE,
                    pChoice(
                            pSeq(
                                 TokenType.FLOAT,
                                 TokenType.DOUBLEDOT,
                                 pMaybe(TokenType.FLOAT)
                            ),
                            pSeq(
                                 TokenType.DOUBLEDOT,
                                 TokenType.FLOAT
                            )
                    )
        );

        /*
         * repetition = optional / one_or_more /
         *              repetition_range / zero_or_more
         * optional = "?"
         */
        p.newNtRule(REPETITION,
                    pConsume(NTConsumers.repetitionConsumer,
                             pChoice(
                                     TokenType.QUESTION_MARK,
                                     ntRule(ONE_OR_MORE),
                                     ntRule(REPETITION_RANGE),
                                     ntRule(ZERO_OR_MORE)
                             )
                    )
        );

        /*
         * repetition_range = "*" spcCmnt? (
         *                             min_max_repetition / min_repetition /
         *                             max_repetition / specific_repetition )
         * specific_repetition = non_neg_integer
         */
        p.newNtRule(REPETITION_RANGE,
                    pSeq(
                         TokenType.ASTERISK,
                         ntRule(SPC_CMNT_Q),
                         pChoice(
                                 ntRule(MIN_MAX_REPETITION),
                                 ntRule(MIN_REPETITION),
                                 ntRule(MAX_REPETITION),
                                 ntRule(NON_NEG_INTEGER)
                            )
                    )
        );

        /*
         * zero_or_more = "*" [ repetition_step ]
         */
        p.newNtRule(ZERO_OR_MORE,
                    pSeq(TokenType.ASTERISK,
                         parsingRuleMaybe(REPETITION_STEP)
                    )
        );

        /*
         * one_or_more = "+" [ repetition_step ]
         */
        p.newNtRule(ONE_OR_MORE,
                    pSeq(
                         TokenType.PLUS,
                         parsingRuleMaybe(REPETITION_STEP)
                    )
        );

        /*
         * min_max_repetition = min_repeat ".." max_repeat
         *                     [ repetition_step ]
         * min_repeat = non_neg_integer
         * max_repeat = non_neg_integer
         */
        p.newNtRule(MIN_MAX_REPETITION,
                    pSeq(
                         ntRule(NON_NEG_INTEGER),
                         TokenType.DOUBLEDOT,
                         ntRule(NON_NEG_INTEGER),
                         pMaybe(ntRule(REPETITION_STEP))
                    )
        );

        /*
         * repetition_step = "%" step_size
         * step_size = non_neg_integer
         */
        p.newNtRule(REPETITION_STEP,
                    pSeq(
                         TokenType.PERCENT,
                         ntRule(NON_NEG_INTEGER)
                    )
        );

        /*
         * min_repetition = min_repeat ".." [ repetition_step ]
         * min_repeat = non_neg_integer
         */
        p.newNtRule(MIN_REPETITION,
                    pSeq(
                         ntRule(NON_NEG_INTEGER),
                         TokenType.DOUBLEDOT,
                         pMaybe(ntRule(REPETITION_STEP))
                    )
        );

        /*
         * max_repetition = ".."  max_repeat [ repetition_step ]
         * max_repeat = non_neg_integer
         */
        p.newNtRule(MAX_REPETITION,
                    pSeq(
                         TokenType.DOUBLEDOT,
                         ntRule(NON_NEG_INTEGER),
                         pMaybe(ntRule(REPETITION_STEP))
                    )
        );

    }
}
