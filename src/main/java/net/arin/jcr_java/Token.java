/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (c) 2013 Cogito Learning Ltd
 * Copyright (c) 2016-2017 ARIN
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package net.arin.jcr_java;

import java.util.ArrayList;

/**
 * A token that is produced by Tokenizer and fed into {@link JcrParser#parse(ArrayList)}.
 *
 * A token consists of a token type, a string that the token was
 * created from and the position in the input string that the token was found.
 */
public class Token implements ParsedObject
{
    /** the token identifier */
    public final TokenType tokenType;
    /** the string that the token was created from */
    public final String sequence;
    /** the position of the token in the input string */
    public final int col;
    /** the line number the token was found on */
    public final int line;

    public Token( TokenType tokenType, String sequence, int col, int line )
    {
        this.tokenType = tokenType;
        this.sequence = sequence;
        this.col = col;
        this.line = line;
    }

    @Override
    public String toString()
    {
        return "Token{" +
            "tokenType=" + tokenType +
            ", sequence='" + sequence + '\'' +
            ", col=" + col +
            ", line=" + line +
            '}';
    }

    @Override
    public void accept( ParsedObject parsedObject )
    {
        //do nothing
    }
}