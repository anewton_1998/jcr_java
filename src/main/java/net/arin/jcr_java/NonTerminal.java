/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import net.arin.jcr_java.JcrParser.Repeat;

import java.util.logging.Logger;

import static net.arin.jcr_java.JcrParser.pMaybe;

/**
 * Something that can be parsed but isn't a token.
 */
@FunctionalInterface
interface NonTerminal extends ParseItem
{
    /**
     * Do some parsing
     * @param parser the parser
     * @return true if parsing worked, otherwise false
     */
    boolean parse( JcrParser parser );

    /**
     * Ntc means Non Terminal Concrete class.
     */
    class Ntc implements NonTerminal
    {
        NonTerminal nonTerminal;
        String desc;

        Ntc( NonTerminal nonTerminal )
        {
            this.nonTerminal = nonTerminal;
            desc = "undescribed";
        }

        public Ntc( ParseItem... parseItems )
        {
            this( new Ntc( (p)-> p.sequence( Repeat.ONCE, parseItems ) ) );
        }

        public Ntc( String desc, ParseItem... parseItems )
        {
            this( new Ntc( (p)-> p.sequence( Repeat.ONCE, parseItems ) ), desc );
        }

        public Ntc( NonTerminal nonTerminal, String desc )
        {
            this.nonTerminal = nonTerminal;
            this.desc = desc;
        }

        @Override
        public boolean parse( JcrParser parser )
        {
            return nonTerminal.parse( parser );
        }

        @Override
        public String toString()
        {
            return "Ntc{" +
                "desc='" + desc + '\'' +
                '}';
        }
    }


    class ParsingRule implements NonTerminal
    {
        Logger log = Logger.getLogger( ParsingRule.class.getName() );
        String name;

        public ParsingRule( String name )
        {
            this.name = name;
        }

        @Override
        public boolean parse( JcrParser parser )
        {
            Token parsingStart = parser.lookahead;
            boolean retval = parser.ntRule(name ).parse(parser );
            if( retval && parsingStart != parser.lookahead )
            {
                parser.lastSuccessfulPointMsg = "Parsing error: successfully parsed up to '" + name + "'. Cannot go further than %s";
                parser.lastSuccessfulPoint = parser.lookahead;
            }
            log.finer( "parsing rule " + name + " returning " + retval );
            return retval;
        }

        @Override
        public String toString()
        {
            return "ParsingRule{" +
                "name='" + name + '\'' +
                '}';
        }
    }

    static ParsingRule ntRule(String name ){
        return new ParsingRule( name );
    }

    static NonTerminal parsingRuleMaybe( String name ){
        return pMaybe( new ParsingRule( name ) );
    }

}
