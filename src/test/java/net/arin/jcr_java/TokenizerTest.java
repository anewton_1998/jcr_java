/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;

/**
 * A test class for {@link Tokenizer}
 */
public class TokenizerTest
{
    @Test
    public void testSimpleArray()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string, [ integer, string ] ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        assertEquals( tokens.get(0).tokenType, TokenType.LEFT_BRACKET );
        assertEquals( tokens.get(1).tokenType.isIgnorable(), true );
        assertEquals( tokens.get(2).tokenType, TokenType.INTEGER_KW );
        assertEquals( tokens.get(3).tokenType, TokenType.COMMA );
        assertEquals( tokens.get(4).tokenType.isIgnorable(), true );
        assertEquals( tokens.get(5).tokenType, TokenType.STRING_KW );
        assertEquals( tokens.get(6).tokenType, TokenType.COMMA );
        assertEquals( tokens.get(7).tokenType.isIgnorable(), true );
        assertEquals( tokens.get(8).tokenType, TokenType.LEFT_BRACKET );
        assertEquals( tokens.get(9).tokenType.isIgnorable(), true );
        assertEquals( tokens.get(10).tokenType, TokenType.INTEGER_KW );
        assertEquals( tokens.get(11).tokenType, TokenType.COMMA );
        assertEquals( tokens.get(12).tokenType.isIgnorable(), true );
        assertEquals( tokens.get(13).tokenType, TokenType.STRING_KW );
        assertEquals( tokens.get(14).tokenType.isIgnorable(), true );
        assertEquals( tokens.get(15).tokenType, TokenType.RIGHT_BRACKET );
        assertEquals( tokens.get(16).tokenType.isIgnorable(), true );
        assertEquals( tokens.get(17).tokenType, TokenType.RIGHT_BRACKET );
        for ( Token token : tokens )
        {
            assertEquals( token.line, 0 );
        }
    }

    @Test
    public void testSimpleArrayAcrossLines()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string,", 0 );
        tokenizer.tokenize( " [ integer, string ]", 1 );
        tokenizer.tokenize( " ]", 2 );
        ArrayList<Token> tokens = tokenizer.getTokens();
        simpleArrayAcrossLinesAssertions( tokens );
    }

    @Test
    public void testSimpleArrayAcrossLines2()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        // whitespace with after /* and before */ are important here
        /*[ integer, string,
         [ integer, string ]
        ] */
        String s;
        s = "[ integer, string,\n" +
            "         [ integer, string ]\n" +
            "        ] ";
        tokenizer.tokenize( s );
        ArrayList<Token> tokens = tokenizer.getTokens();
        simpleArrayAcrossLinesAssertions( tokens );
    }

    @Test
    public void testSpacesAndComment()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        /*

        ;comment
        [ integer, string, ; another comment
         [ integer, string ]
         ; [ integer, string ]
        ]

        */
        String s;
        s = "\n" +
            "        ;comment\n" +
            "        [ integer, string, ; another comment\n" +
            "         [ integer, string ]\n" +
            "         ; [ integer, string ]\n" +
            "        ]\n" +
            "\n" +
            " ";
        tokenizer.tokenize( s );
        tokenizer.getTokens();
    }

    private void simpleArrayAcrossLinesAssertions( ArrayList<Token> tokens )
    {
        assertEquals( tokens.get(0).tokenType, TokenType.LEFT_BRACKET );
        assertEquals( tokens.get(1).tokenType.isIgnorable(), true );
        assertEquals( tokens.get(2).tokenType, TokenType.INTEGER_KW );
        assertEquals( tokens.get(3).tokenType, TokenType.COMMA );
        assertEquals( tokens.get(4).tokenType.isIgnorable(), true );
        assertEquals( tokens.get(5).tokenType, TokenType.STRING_KW );
        assertEquals( tokens.get(6).tokenType, TokenType.COMMA );
        assertEquals( tokens.get(7).tokenType.isIgnorable(), true );
        assertEquals( tokens.get(8).tokenType, TokenType.LEFT_BRACKET );
        assertEquals( tokens.get(9).tokenType.isIgnorable(), true );
        assertEquals( tokens.get(10).tokenType, TokenType.INTEGER_KW );
        assertEquals( tokens.get(11).tokenType, TokenType.COMMA );
        assertEquals( tokens.get(12).tokenType.isIgnorable(), true );
        assertEquals( tokens.get(13).tokenType, TokenType.STRING_KW );
        assertEquals( tokens.get(14).tokenType.isIgnorable(), true );
        assertEquals( tokens.get(15).tokenType, TokenType.RIGHT_BRACKET );
        assertEquals( tokens.get(16).tokenType.isIgnorable(), true );
        assertEquals( tokens.get(17).tokenType, TokenType.RIGHT_BRACKET );
        int i = 0;
        for ( Token token : tokens )
        {
            if( i <= 6 )
            {
                assertEquals( token.line, 0 );
            }
            else if( i > 6 && i <= 15 )
            {
                assertEquals( token.line, 1 );
            }
            else if( i > 15 )
            {
                assertEquals( token.line, 2 );
            }
            i++;
        }
    }

    @Test
    public void testSimpleTokens()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        /*

        integer
        string
        null
        boolean
        true
        false
        float
        double
        email
        phone
        ipv4
        ipv6
        ipaddr
        datetime
        date
        time

        */
        String s;
        s = "\n" +
            "        integer\n" +
            "        string\n" +
            "        null\n" +
            "        boolean\n" +
            "        true\n" +
            "        false\n" +
            "        float\n" +
            "        double\n" +
            "        email\n" +
            "        phone\n" +
            "        ipv4\n" +
            "        ipv6\n" +
            "        ipaddr\n" +
            "        datetime\n" +
            "        date\n" +
            "        time\n" +
            "\n" +
            " ";
        tokenizer.tokenize( s );
        tokenizer.getTokens();
    }

    @Test
    public void testQString()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "\"foo\"" );
        assertEquals( tokenizer.getTokens().get( 0 ).tokenType, TokenType.QSTRING );

        tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "\"f\\\"o\\\"o\"" );
        assertEquals( tokenizer.getTokens().get( 0 ).tokenType, TokenType.QSTRING );
    }

    @Test
    public void testRegex()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "/foo/" );
        assertEquals( tokenizer.getTokens().get( 0 ).tokenType, TokenType.REGEX );

        tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "/f\\o\\o/" );
        assertEquals( tokenizer.getTokens().get( 0 ).tokenType, TokenType.REGEX );

        tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "/foo/i" );
        assertEquals( tokenizer.getTokens().size(), 1 );
        assertEquals( tokenizer.getTokens().get( 0 ).tokenType, TokenType.REGEX );

        tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "/f\\o\\o/i" );
        assertEquals( tokenizer.getTokens().size(), 1 );
        assertEquals( tokenizer.getTokens().get( 0 ).tokenType, TokenType.REGEX );
    }

    @Test
    public void testPosIntegerFloatMinusZero()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "123" );
        assertEquals( tokenizer.getTokens().get( 0 ).tokenType, TokenType.POS_INTEGER );

        tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "123.01" );
        assertEquals( tokenizer.getTokens().get( 0 ).tokenType, TokenType.FLOAT );

        tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "-123.01" );
        assertEquals( tokenizer.getTokens().get( 0 ).tokenType, TokenType.FLOAT );

        tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "-123" );
        assertEquals( tokenizer.getTokens().get( 0 ).tokenType, TokenType.MINUS );
        assertEquals( tokenizer.getTokens().get( 1 ).tokenType, TokenType.POS_INTEGER );

        tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "0" );
        assertEquals( tokenizer.getTokens().get( 0 ).tokenType, TokenType.ZERO );

        tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "0.01" );
        assertEquals( tokenizer.getTokens().get( 0 ).tokenType, TokenType.FLOAT );
    }
}
