/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * Tests for parsing and printing object rules.
 * Unlike the previous generation of parsing tests, these tests can rely on toString
 * because the ToStringTest has proven out the previous aspects of the object hierachy.
 * Using toString should make the tests more readable and easier to maintain.
 */
public class ObjectRuleRuleCreationTest
{
    @BeforeClass
    public void setup()
    {
        Level level = Level.INFO;
        //Level level = Level.FINE;
        //Level level = Level.FINEST;
        Logger rootLogger = Logger.getLogger("" );
        for ( Handler handler : rootLogger.getHandlers() )
        {
            handler.setLevel( level );
        }
        rootLogger.setLevel( level );

    }

    @AfterClass
    public void tearDown()
    {
        LogManager.getLogManager().reset();
    }

    @Test
    public void testDefaultRootObject()
    {
        // { "foo" : string }
        RuleSet r = JcrParser.parse( "{ \"foo\" : string }" );
        assertTrue( r.getSub(0) instanceof ObjectRule);
        assertEquals( r.getSub(0).toString(), "{ \"foo\" : string }" );
    }

    @Test
    public void testObjectAssignment()
    {
        // $foo = { "foo" : string }
        RuleSet r = JcrParser.parse( "$foo = { \"foo\" : string }" );
        assertEquals( r.getSub(0).toString(), "$foo = { \"foo\" : string }" );
    }

    @Test
    public void testObjectInArray()
    {
        // [ { "foo" : string } ]
        RuleSet r = JcrParser.parse( "[ { \"foo\" : string } ]" );
        assertEquals( r.getSub(0).toString(), "[ { \"foo\" : string } ]" );
    }

    @Test
    public void testObjectInMember()
    {
        // $foo = "foo" : { "bar" : string }
        RuleSet r = JcrParser.parse( "$foo = \"foo\" : { \"bar\" : string }" );
        assertEquals( r.getSub(0).toString(), "$foo = \"foo\" : { \"bar\" : string }" );
    }

    @Test
    public void testMultipleMembersInObject()
    {
        // { "foo" : string , "bar" : integer }
        RuleSet r = JcrParser.parse( "{ \"foo\" : string , \"bar\" : integer }" );
        assertEquals( r.getSub(0 ).toString(), "{ \"foo\" : string , \"bar\" : integer }" );
    }

    @Test
    public void testTargetRulesInObject()
    {
        // { "foo" : string , $bar }
        RuleSet r = JcrParser.parse( "{ \"foo\" : string , $bar }" );
        assertEquals( r.getSub(0 ).toString(), "{ \"foo\" : string , $bar }" );
    }

    @Test
    public void testObjectInObject()
    {
        // { "foo" : { "bar" : string } }
        RuleSet r = JcrParser.parse( "{ \"foo\" : { \"bar\" : string } }" );
        assertEquals( r.getSub(0).toString(), "{ \"foo\" : { \"bar\" : string } }" );
    }

    @Test
    public void testObjectAnnotations()
    {
        // [ @{not} { "foo" : string } ]
        RuleSet r = JcrParser.parse( "[ @{not} { \"foo\" : string } ]" );
        assertEquals( r.getSub(0).toString(), "[ @{not} { \"foo\" : string } ]" );
        assertNotNull( ((ObjectRule)r.getSub(0).getSub(0).getSub(0)).not );
    }

    @Test
    public void testEmptyObject()
    {
        // { }
        RuleSet r = JcrParser.parse( "{ }" );
        assertEquals( r.getSub(0).toString(), "{ }" );
    }

    @Test
    public void testObjectWithRegex()
    {
        // { /^bar/ : string }
        RuleSet r = JcrParser.parse( "{ /^bar/ : string }" );
        assertEquals( r.getSub(0).toString(), "{ /^bar/ : string }" );
    }

    @Test
    public void testObjectWithMultipleCombiners()
    {
        // { /^bar/ : string , /^foo/ : integer }
        RuleSet r = JcrParser.parse( "{ /^bar/ : string , /^foo/ : integer }" );
        assertEquals( r.getSub(0).toString(), "{ /^bar/ : string , /^foo/ : integer }" );

        // { /^bar/ : string | /^foo/ : integer }
        r = JcrParser.parse( "{ /^bar/ : string | /^foo/ : integer }" );
        assertEquals( r.getSub(0).toString(), "{ /^bar/ : string | /^foo/ : integer }" );

        // { /^bar/ : string , /^foo/ : integer , "one hundred" : 100 }
        r = JcrParser.parse( "{ /^bar/ : string , /^foo/ : integer , \"one hundred\" : 100 }" );
        assertEquals( r.getSub(0).toString(), "{ /^bar/ : string , /^foo/ : integer , \"one hundred\" : 100 }" );

        // { /^bar/ : string | /^foo/ : integer | "one hundred" : 100 }
        r = JcrParser.parse( "{ /^bar/ : string | /^foo/ : integer | \"one hundred\" : 100 }" );
        assertEquals( r.getSub(0).toString(), "{ /^bar/ : string | /^foo/ : integer | \"one hundred\" : 100 }" );
    }

    @Test
    public void testObjectWithRepetitions()
    {
        // { /^bar/ : string ? , /^foo/ : integer *1..5 }
        RuleSet r = JcrParser.parse( "{ /^bar/ : string ? , /^foo/ : integer *1..5 }" );
        assertEquals( r.getSub(0).toString(), "{ /^bar/ : string ? , /^foo/ : integer *1..5 }" );

        // { /^bar/ : string ? | /^foo/ : integer *1..5 }
        r = JcrParser.parse( "{ /^bar/ : string ? | /^foo/ : integer *1..5 }" );
        assertEquals( r.getSub(0).toString(), "{ /^bar/ : string ? | /^foo/ : integer *1..5 }" );

        // { /^bar/ : string ? , /^foo/ : integer *1..5 , "one hundred" : 100 }
        r = JcrParser.parse( "{ /^bar/ : string ? , /^foo/ : integer *1..5 , \"one hundred\" : 100 }" );
        assertEquals( r.getSub(0).toString(), "{ /^bar/ : string ? , /^foo/ : integer *1..5 , \"one hundred\" : 100 }" );

        // { /^bar/ : string ? | /^foo/ : integer *1..5 | "one hundred" : 100 }
        r = JcrParser.parse( "{ /^bar/ : string ? | /^foo/ : integer *1..5 | \"one hundred\" : 100 }" );
        assertEquals( r.getSub(0).toString(), "{ /^bar/ : string ? | /^foo/ : integer *1..5 | \"one hundred\" : 100 }" );
    }
}
