/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static org.testng.Assert.assertEquals;

/**
 * Tests the parsing and printing of group rules.
 * Unlike the previous generation of parsing tests, these tests can rely on toString
 * because the ToStringTest has proven out the previous aspects of the object hierachy.
 * Using toString should make the tests more readable and easier to maintain.
 */
public class GroupRuleCreationTest
{
    @BeforeClass
    public void setup()
    {
        Level level = Level.INFO;
        //Level level = Level.FINE;
        //Level level = Level.FINEST;
        Logger rootLogger = Logger.getLogger("" );
        for ( Handler handler : rootLogger.getHandlers() )
        {
            handler.setLevel( level );
        }
        rootLogger.setLevel( level );

    }

    @AfterClass
    public void tearDown()
    {
        LogManager.getLogManager().reset();
    }

    @Test
    public void testDefaultRootGroup()
    {
        // ( [ string ] , [ integer ] )
        RuleSet r = JcrParser.parse( "( [ string ] , [ integer ] )" );
        assertEquals( r.getSub(0).toString(), "( [ string ] , [ integer ] )" );
    }

    @Test
    public void testGroupWithTargetRules()
    {
        // ( $t1 , $t2 )
        RuleSet r = JcrParser.parse( "( $t1 , $t2 )" );
        assertEquals( r.getSub(0).toString(), "( $t1 , $t2 )" );

        // ( $t1 | $t2 )
        r = JcrParser.parse( "( $t1 | $t2 )" );
        assertEquals( r.getSub(0).toString(), "( $t1 | $t2 )" );
    }

    @Test( expectedExceptions = { ParsingException.class } )
    public void testEmptyGroup()
    {
        // ( )
        RuleSet r = JcrParser.parse( "( )" );
    }

    @Test
    public void testGroupAssignment()
    {
        // $foo = ( [ string ] , [ integer ] )
        RuleSet r = JcrParser.parse( "$foo = ( [ string ] , [ integer ] )" );
        assertEquals( r.getSub(0).toString(), "$foo = ( [ string ] , [ integer ] )" );

        // $foo = ( [ string ] | [ integer ] )
        r = JcrParser.parse( "$foo = ( [ string ] | [ integer ] )" );
        assertEquals( r.getSub(0).toString(), "$foo = ( [ string ] | [ integer ] )" );

        // $foo = ( { "foo" : string , "bar" : integer } | { "fuzz" : boolean , "buzz" : float } )
        r = JcrParser.parse( "$foo = ( { \"foo\" : string , \"bar\" : integer } | { \"fuzz\" : boolean , \"buzz\" : float } )" );
        assertEquals( r.getSub(0).toString(), "$foo = ( { \"foo\" : string , \"bar\" : integer } | { \"fuzz\" : boolean , \"buzz\" : float } )" );


        // $foo = ( { "foo" : string , "bar" : integer } , { "fuzz" : boolean , "buzz" : float } )
        r = JcrParser.parse( "$foo = ( { \"foo\" : string , \"bar\" : integer } , { \"fuzz\" : boolean , \"buzz\" : float } )" );
        assertEquals( r.getSub(0).toString(), "$foo = ( { \"foo\" : string , \"bar\" : integer } , { \"fuzz\" : boolean , \"buzz\" : float } )" );
    }

    @Test
    public void testGroupTypeAssignment()
    {
        // $foo =: ( 2 | 3 )
        RuleSet r = JcrParser.parse( "$foo =: ( 2 | 3 )" );
        assertEquals( r.getSub(0).toString(), "$foo = ( 2 | 3 )" ); //note, = not =:

        // $foo = ( 2 | 3 )
        r = JcrParser.parse( "$foo = ( 2 | 3 )" );
        assertEquals( r.getSub(0).toString(), "$foo = ( 2 | 3 )" );
    }

    @Test
    public void testGroupsInGroups()
    {
        // ( [ string ] , ( [ integer ] ) )
        RuleSet r = JcrParser.parse( "( [ string ] , ( [ integer ] ) )" );
        assertEquals( r.getSub(0).toString(), "( [ string ] , ( [ integer ] ) )" );

        // ( [ string ] , ( [ integer , boolean ] ) )
        r = JcrParser.parse( "( [ string ] , ( [ integer , boolean ] ) )" );
        assertEquals( r.getSub(0).toString(), "( [ string ] , ( [ integer , boolean ] ) )" );

        // ( [ string ] , ( [ integer | boolean ] ) )
        r = JcrParser.parse( "( [ string ] , ( [ integer | boolean ] ) )" );
        assertEquals( r.getSub(0).toString(), "( [ string ] , ( [ integer | boolean ] ) )" );
    }

    @Test
    public void testGroupsInArrays()
    {
        // [ integer , ( string | boolean ) ]
        RuleSet r = JcrParser.parse( "[ integer , ( string | boolean ) ]" );
        assertEquals( r.getSub(0).toString(), "[ integer , ( string | boolean ) ]" );

        // [ integer , ( string , boolean ) ]
        r = JcrParser.parse( "[ integer , ( string , boolean ) ]" );
        assertEquals( r.getSub(0).toString(), "[ integer , ( string , boolean ) ]" );
    }

    @Test
    public void testGroupsInObjects()
    {
        // { "foo" : string , ( "bar" : integer ) }
        RuleSet r = JcrParser.parse( "{ \"foo\" : string , ( \"bar\" : integer ) }" );
        assertEquals( r.getSub(0).toString(), "{ \"foo\" : string , ( \"bar\" : integer ) }" );

        // { "foo" : string , ( "bar" : integer , "fuzz" : boolean ) }
        r = JcrParser.parse( "{ \"foo\" : string , ( \"bar\" : integer , \"fuzz\" : boolean ) }" );
        assertEquals( r.getSub(0).toString(), "{ \"foo\" : string , ( \"bar\" : integer , \"fuzz\" : boolean ) }" );

        // { "foo" : string , ( "bar" : integer | "fuzz" : boolean ) }
        r = JcrParser.parse( "{ \"foo\" : string , ( \"bar\" : integer | \"fuzz\" : boolean ) }" );
        assertEquals( r.getSub(0).toString(), "{ \"foo\" : string , ( \"bar\" : integer | \"fuzz\" : boolean ) }" );
    }
}
