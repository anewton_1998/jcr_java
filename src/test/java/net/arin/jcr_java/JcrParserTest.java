/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import net.arin.jcr_java.JcrParser.Repeat;
import net.arin.jcr_java.NonTerminal.Ntc;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;

import static net.arin.jcr_java.JcrParser.pChoice;
import static net.arin.jcr_java.JcrParser.pConsume;
import static net.arin.jcr_java.JcrParser.pSeq;
import static net.arin.jcr_java.NonTerminal.ntRule;
import static net.arin.jcr_java.TokenType.INTEGER_KW;
import static net.arin.jcr_java.TokenType.STRING_KW;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.expectThrows;

/**
 * A test class for {@link JcrParser}
 */
public class JcrParserTest
{
    @Test
    public void testNextToken()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string, [ integer, string ] ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );

        assertEquals( jcrParser.lookahead.tokenType, TokenType.LEFT_BRACKET );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, INTEGER_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.STRING_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.LEFT_BRACKET );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, INTEGER_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.STRING_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.RIGHT_BRACKET );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.RIGHT_BRACKET );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.END_OF_FILE );
    }

    @Test
    public void testSavePoint()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string, [ integer, string ] ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );

        assertEquals( jcrParser.lookahead.tokenType, TokenType.LEFT_BRACKET );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, INTEGER_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.STRING_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.LEFT_BRACKET );
        jcrParser.savePoint();
        jcrParser.nextToken();

        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, INTEGER_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.STRING_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.RIGHT_BRACKET );
        jcrParser.restorePoint();

        assertEquals( jcrParser.lookahead.tokenType, TokenType.LEFT_BRACKET );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, INTEGER_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.STRING_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.RIGHT_BRACKET );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.RIGHT_BRACKET );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.END_OF_FILE );
    }

    @Test
    public void testRemoveSavePoint()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string, [ integer, string ] ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );

        assertEquals( jcrParser.lookahead.tokenType, TokenType.LEFT_BRACKET );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, INTEGER_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.STRING_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.LEFT_BRACKET );
        jcrParser.savePoint();
        jcrParser.nextToken();

        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, INTEGER_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.STRING_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.RIGHT_BRACKET );
        jcrParser.removeSavePoint();

        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.RIGHT_BRACKET );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.END_OF_FILE );
    }

    @Test
    public void testNestedSavePoint()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string, [ integer, string ] ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );

        assertEquals( jcrParser.lookahead.tokenType, TokenType.LEFT_BRACKET );
        jcrParser.savePoint();
        jcrParser.nextToken();

        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, INTEGER_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.STRING_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.LEFT_BRACKET );
        jcrParser.savePoint();
        jcrParser.nextToken();

        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, INTEGER_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.STRING_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.RIGHT_BRACKET );
        jcrParser.removeSavePoint();

        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.RIGHT_BRACKET );
        jcrParser.nextToken();

        jcrParser.restorePoint();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.LEFT_BRACKET );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, INTEGER_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.STRING_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.LEFT_BRACKET );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, INTEGER_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.COMMA );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.STRING_KW );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.RIGHT_BRACKET );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.SPACES );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.RIGHT_BRACKET );
        jcrParser.nextToken();
        assertEquals( jcrParser.lookahead.tokenType, TokenType.END_OF_FILE );
    }

    /**
     * Simply implements a choice between an integer keyword and string keyword
     */
    class TestPrimitive implements NonTerminal
    {
        @Override
        public boolean parse( JcrParser parser )
        {
            return parser.choice( Repeat.ONCE, INTEGER_KW, TokenType.STRING_KW );
        }
    }

    @Test
    public void testChoiceSequenceOnce()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );

        Ntc spaces_o = new Ntc( (p) -> p.choice( Repeat.OPTIONAL, TokenType.SPACES ) );
        Ntc combiner = new Ntc( ( p ) -> p.choice( Repeat.ONCE, TokenType.COMMA, TokenType.PIPE ) );

        TestPrimitive tp = new TestPrimitive();
        boolean retval = jcrParser.sequence( Repeat.ONCE,
            TokenType.LEFT_BRACKET,
            spaces_o,
            tp,
            spaces_o,
            combiner,
            spaces_o,
            tp,
            spaces_o,
            TokenType.RIGHT_BRACKET );

        assertTrue( retval );
    }

    @Test
    public void testChoiceSequenceOnceFail()
    {
        /*
            This fails because it should expect [ integer, string ] not [ integer, string, integer ]
         */
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string, integer ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );

        Ntc spaces_o = new Ntc( (p) -> p.choice( Repeat.OPTIONAL, TokenType.SPACES ) );
        Ntc combiner = new Ntc( ( p ) -> p.choice( Repeat.ONCE, TokenType.COMMA, TokenType.PIPE ) );

        TestPrimitive tp = new TestPrimitive();
        boolean retval = jcrParser.sequence( Repeat.ONCE,
            TokenType.LEFT_BRACKET,
            spaces_o,
            tp,
            spaces_o,
            combiner,
            spaces_o,
            tp,
            spaces_o,
            TokenType.RIGHT_BRACKET );

        assertFalse( retval );
    }

    @Test
    public void testChoiceSequenceZeroOrMore()
    {
        // test with one
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );

        TestPrimitive tp = new TestPrimitive();
        Ntc spaces_o = new Ntc( (p) -> p.choice( Repeat.OPTIONAL, TokenType.SPACES ) );
        Ntc combiner = new Ntc( (p) -> p.choice( Repeat.ONCE, TokenType.COMMA, TokenType.PIPE ) );
        Ntc items = new Ntc( (p) -> p.sequence( Repeat.ZERO_OR_MORE, combiner, spaces_o, tp ) );

        boolean retval = jcrParser.sequence( Repeat.ONCE,
            TokenType.LEFT_BRACKET,
            spaces_o,
            tp,
            spaces_o,
            items,
            spaces_o,
            TokenType.RIGHT_BRACKET );
        assertTrue( retval );

        //test zero
        tokenizer.tokenize( "[ integer ]" );
        tokens = tokenizer.getTokens();
        jcrParser.setupParse( tokens );

        retval = jcrParser.sequence( Repeat.ONCE,
            TokenType.LEFT_BRACKET,
            spaces_o,
            tp,
            spaces_o,
            items,
            spaces_o,
            TokenType.RIGHT_BRACKET );
        assertTrue( retval );

        //test with two
        tokenizer.tokenize( "[ integer, string, integer, string ]" );
        tokens = tokenizer.getTokens();
        jcrParser.setupParse( tokens );

        retval = jcrParser.sequence( Repeat.ONCE,
            TokenType.LEFT_BRACKET,
            spaces_o,
            tp,
            spaces_o,
            items,
            spaces_o,
            TokenType.RIGHT_BRACKET );
        assertTrue( retval );
    }

    @Test
    public void testChoiceSequenceOneOrMore()
    {
        // test with one
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );

        TestPrimitive tp = new TestPrimitive();
        Ntc spaces_o = new Ntc( (p) -> p.choice( Repeat.OPTIONAL, TokenType.SPACES ) );
        Ntc combiner = new Ntc( (p) -> p.choice( Repeat.ONCE, TokenType.COMMA, TokenType.PIPE ) );
        Ntc items = new Ntc( (p) -> p.sequence( Repeat.ONE_OR_MORE, combiner, spaces_o, tp ) );

        boolean retval = jcrParser.sequence( Repeat.ONCE,
            TokenType.LEFT_BRACKET,
            spaces_o,
            tp,
            spaces_o,
            items,
            spaces_o,
            TokenType.RIGHT_BRACKET );
        assertTrue( retval );

        //test zero
        tokenizer.tokenize( "[ integer ]" );
        tokens = tokenizer.getTokens();
        jcrParser.setupParse( tokens );

        retval = jcrParser.sequence( Repeat.ONCE,
            TokenType.LEFT_BRACKET,
            spaces_o,
            tp,
            spaces_o,
            items,
            spaces_o,
            TokenType.RIGHT_BRACKET );
        assertFalse( retval );

        //test with two
        tokenizer.tokenize( "[ integer, string, integer, string ]" );
        tokens = tokenizer.getTokens();
        jcrParser.setupParse( tokens );

        retval = jcrParser.sequence( Repeat.ONCE,
            TokenType.LEFT_BRACKET,
            spaces_o,
            tp,
            spaces_o,
            items,
            spaces_o,
            TokenType.RIGHT_BRACKET );
        assertTrue( retval );
    }

    @Test
    public void testChoiceSequenceOptional()
    {
        // test with one
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );

        TestPrimitive tp = new TestPrimitive();
        Ntc spaces_o = new Ntc( (p) -> p.choice( Repeat.OPTIONAL, TokenType.SPACES ) );
        Ntc combiner = new Ntc( (p) -> p.choice( Repeat.ONCE, TokenType.COMMA, TokenType.PIPE ) );
        Ntc items = new Ntc( (p) -> p.sequence( Repeat.OPTIONAL, combiner, spaces_o, tp ) );

        boolean retval = jcrParser.sequence( Repeat.ONCE,
            TokenType.LEFT_BRACKET,
            spaces_o,
            tp,
            spaces_o,
            items,
            spaces_o,
            TokenType.RIGHT_BRACKET );
        assertTrue( retval );

        //test zero
        tokenizer.tokenize( "[ integer ]" );
        tokens = tokenizer.getTokens();
        jcrParser.setupParse( tokens );

        retval = jcrParser.sequence( Repeat.ONCE,
            TokenType.LEFT_BRACKET,
            spaces_o,
            tp,
            spaces_o,
            items,
            spaces_o,
            TokenType.RIGHT_BRACKET );
        assertTrue( retval );

        //test with two
        tokenizer.tokenize( "[ integer, string, integer ]" );
        tokens = tokenizer.getTokens();
        jcrParser.setupParse( tokens );

        retval = jcrParser.sequence( Repeat.ONCE,
            TokenType.LEFT_BRACKET,
            spaces_o,
            tp,
            spaces_o,
            items,
            spaces_o,
            TokenType.RIGHT_BRACKET );
        assertFalse( retval );
    }


    @Test
    public void testNestedChoiceSequence()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string, integer, [ integer, string ] ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );

        class TestArray implements NonTerminal
        {
            class TestItem implements NonTerminal
            {
                @Override
                public boolean parse( JcrParser parser )
                {
                    return parser.choice( Repeat.ONCE,
                        INTEGER_KW,
                        TokenType.STRING_KW,
                        new TestArray() );
                }
            }

            TestItem ti = new TestItem();
            Ntc spaces_o = new Ntc( (p) -> p.choice( Repeat.OPTIONAL, TokenType.SPACES ) );
            Ntc combiner = new Ntc( (p) -> p.choice( Repeat.ONCE, TokenType.COMMA, TokenType.PIPE ) );
            Ntc items = new Ntc( (p) -> p.sequence( Repeat.ZERO_OR_MORE, combiner, spaces_o, ti ) );

            @Override
            public boolean parse( JcrParser parser )
            {
                return jcrParser.sequence( Repeat.ONCE,
                    TokenType.LEFT_BRACKET,
                    spaces_o,
                    ti,
                    spaces_o,
                    items,
                    spaces_o,
                    TokenType.RIGHT_BRACKET );
            }
        }
        TestArray ta = new TestArray();

        assertTrue( ta.parse( jcrParser ));
    }

    @Test
    public void testParsingRules()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string, integer, [ integer, string ] ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );
        makeTestParsingRules( jcrParser );

        assertTrue( jcrParser.ntRule("array" ).parse(jcrParser ) );

        tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "integer, string, integer, [ integer, string ]" );
        tokens = tokenizer.getTokens();
        jcrParser.setupParse( tokens );
        makeTestParsingRules( jcrParser );

        assertFalse( jcrParser.ntRule("array" ).parse(jcrParser ) );
    }

    private void makeTestParsingRules( JcrParser jcrParser )
    {
        jcrParser.newNtRule("array", JcrParser.pSeq(Repeat.ONCE,
                                                    TokenType.LEFT_BRACKET,
                                                    ntRule("spaces_o" ),
                                                    ntRule("ti" ),
                                                    ntRule("spaces_o" ),
                                                    ntRule("items" ),
                                                    ntRule("spaces_o" ),
                                                    TokenType.RIGHT_BRACKET
            )
        );

        jcrParser.newNtRule("spaces_o", pChoice(Repeat.OPTIONAL, TokenType.SPACES ) );
        jcrParser.newNtRule("combiner", pChoice(Repeat.ONCE, TokenType.COMMA, TokenType.PIPE ) );
        jcrParser.newNtRule("items", JcrParser.pSeq(Repeat.ZERO_OR_MORE,
                                                    ntRule("combiner"),
                                                    ntRule("spaces_o"),
                                                    ntRule("ti")
            )
        );
        jcrParser.newNtRule("ti", pChoice(Repeat.ONCE,
                                          TokenType.INTEGER_KW,
                                          TokenType.STRING_KW,
                                          ntRule("array" )
            )
        );
    }

    @Test
    public void testConsume()
    {
        ArrayList<String> events = new ArrayList<>();

        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string, integer, [ integer, string ] ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );

        jcrParser.newNtRule("array", JcrParser.pSeq(Repeat.ONCE,
                                                    pConsume( (c,p) -> events.add( "start array" ), TokenType.LEFT_BRACKET ),
                                                    ntRule("spaces_o" ),
                                                    ntRule("ti" ),
                                                    ntRule("spaces_o" ),
                                                    ntRule("items" ),
                                                    ntRule("spaces_o" ),
                                                    pConsume( (c,p) -> events.add( "end array" ), TokenType.RIGHT_BRACKET )
            )
        );

        jcrParser.newNtRule("spaces_o", pChoice(Repeat.OPTIONAL, TokenType.SPACES ) );
        jcrParser.newNtRule("combiner", pChoice(Repeat.ONCE, TokenType.COMMA, TokenType.PIPE ) );
        jcrParser.newNtRule("items", JcrParser.pSeq(Repeat.ZERO_OR_MORE,
                                                    ntRule("combiner"),
                                                    ntRule("spaces_o"),
                                                    ntRule("ti")
            )
        );
        jcrParser.newNtRule("ti",
                            pConsume( (c,p) -> events.add( "array item" ),
                pChoice(Repeat.ONCE,
                        TokenType.INTEGER_KW,
                        TokenType.STRING_KW,
                        ntRule("array" )
                )
            )
        );

        assertTrue( jcrParser.ntRule("array" ).parse(jcrParser ) );
        assertEquals( events.get(0), "start array" );
        assertEquals( events.get(1), "array item" );
        assertEquals( events.get(2), "array item" );
        assertEquals( events.get(3), "array item" );
        assertEquals( events.get(4), "start array" );
        assertEquals( events.get(5), "array item" );
        assertEquals( events.get(6), "array item" );
        assertEquals( events.get(7), "end array" );
        assertEquals( events.get(8), "array item" );
        assertEquals( events.get(9), "end array" );
    }

    @Test
    public void testParsedObjectsStack()
    {
        class TestParsedObject implements ParsedObject{
            String name;
            ParsedObject child = null;

            public TestParsedObject( String name )
            {
                this.name = name;
            }

            @Override
            public String toString()
            {
                return name;
            }

            @Override
            public void accept( ParsedObject parsedObject )
            {
                child = parsedObject;
            }
        }

        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string, integer, [ integer, string ] ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );

        jcrParser.newNtRule("array", JcrParser.pSeq(Repeat.ONCE,
                                                    pConsume( (c,p) -> p.push( new TestParsedObject( "array" ) ), TokenType.LEFT_BRACKET ),
                                                    ntRule("spaces_o" ),
                                                    ntRule("ti" ),
                                                    ntRule("spaces_o" ),
                                                    ntRule("items" ),
                                                    ntRule("spaces_o" ),
                                                    TokenType.RIGHT_BRACKET
            )
        );

        jcrParser.newNtRule("spaces_o", pChoice(Repeat.OPTIONAL, TokenType.SPACES ) );
        jcrParser.newNtRule("combiner", pChoice(Repeat.ONCE, TokenType.COMMA, TokenType.PIPE ) );
        jcrParser.newNtRule("items", JcrParser.pSeq(Repeat.ZERO_OR_MORE,
                                                    ntRule("combiner"),
                                                    ntRule("spaces_o"),
                                                    ntRule("ti")
            )
        );
        jcrParser.newNtRule("primitive",
                            pConsume( (c,p) -> p.push( new TestParsedObject( "primitive" ) ),
                pChoice( Repeat.ONCE,
                    TokenType.INTEGER_KW,
                    TokenType.STRING_KW
                )
            )
        );
        jcrParser.newNtRule("ti",
                            pConsume( (c,p) -> p.popAndConsume(),
                pChoice(Repeat.ONCE,
                        ntRule("primitive" ),
                        ntRule("array" )
                )
            )
        );

        assertTrue( jcrParser.ntRule("array" ).parse(jcrParser ) );
        assertEquals( jcrParser.parsedObjects.size(), 1 );
        assertEquals( jcrParser.parsedObjects.get(0).toString(), "array" );
        TestParsedObject child = ( TestParsedObject ) ((TestParsedObject)jcrParser.parsedObjects.get(0)).child;
        assertEquals( child.toString(), "array" );
        child = ( TestParsedObject ) child.child;
        assertEquals( child.toString(), "primitive" );
    }

    @Test()
    public void testOnSuccess()
    {
        /**
         * Marker class to use generics so the compiler doesn't throw unchecked warnings.
         */
        class ArrayThing {}
        Collection<ArrayThing> topLevelCollection = new ArrayList<>();

        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string, integer, [ integer, string ] ]" );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );


        class Primitive extends ArrayThing
        {
            Token token;

            public Primitive( Token token )
            {
                this.token = token;
            }
        }

        class PrimitiveNotTerminal implements NonTerminal
        {
            Collection<ArrayThing> collection;

            public PrimitiveNotTerminal( Collection<ArrayThing> collection )
            {
                this.collection = collection;
            }

            class PrimitiveConsumer implements Consumer<ArrayList<Token>>
            {
                @Override
                public void accept( ArrayList<Token> tokens )
                {
                    Token token = tokens.get( 0 );
                    Primitive p = new Primitive( token );
                    collection.add( p );
                }
            }


            @Override
            public boolean parse( JcrParser parser )
            {
                return parser.choice(
                    Repeat.ONCE,
                    new PrimitiveConsumer(),
                    INTEGER_KW,
                    TokenType.STRING_KW );
            }
        }

        class Array extends ArrayThing
        {
            Collection<ArrayThing> subordinates;

            public Array()
            {
                this.subordinates = new ArrayList<ArrayThing>();
            }
        }

        class ArrayNonTerminal implements NonTerminal
        {
            Collection<ArrayThing> parentCollection;
            Array array;

            public ArrayNonTerminal( Collection<ArrayThing> arrays )
            {
                this.parentCollection = arrays;
                this.array = new Array();
            }

            Ntc spaces_o = new Ntc( (p) -> p.choice( Repeat.OPTIONAL, TokenType.SPACES ) );
            Ntc combiner = new Ntc( (p) -> p.choice( Repeat.ONCE, TokenType.COMMA, TokenType.PIPE ) );
            Ntc items = new Ntc( (p) -> p.sequence( Repeat.ZERO_OR_MORE, combiner, spaces_o, new ArrayItem() ) );

            @Override
            public boolean parse( JcrParser parser )
            {
                return parser.sequence( Repeat.ONCE,
                    new ArrayConsumer(),
                    TokenType.LEFT_BRACKET,
                    spaces_o,
                    new ArrayItem(),
                    spaces_o,
                    items,
                    spaces_o,
                    TokenType.RIGHT_BRACKET );
            }

            class ArrayConsumer implements Consumer<ArrayList<Token>>
            {
                @Override
                public void accept( ArrayList<Token> tokens )
                {
                    parentCollection.add( array );
                }
            }


            class ArrayItem implements NonTerminal
            {
                @Override
                public boolean parse( JcrParser parser )
                {
                    return parser.choice( Repeat.ONCE,
                        new PrimitiveNotTerminal( array.subordinates ),
                        new ArrayNonTerminal( array.subordinates ) );
                }
            }
        }

        ArrayNonTerminal arrayNonTerminal = new ArrayNonTerminal( topLevelCollection );

        assertTrue( arrayNonTerminal.parse( jcrParser ) );
        assertEquals( topLevelCollection.size(), 1 );
        Array array = arrayNonTerminal.array;
        assertNotNull( array );
        assertEquals( array.subordinates.size(), 4 );
        ArrayList subs = (ArrayList)array.subordinates;
        assertEquals( ((Primitive)subs.get(0)).token.tokenType, INTEGER_KW );
        assertEquals( ((Primitive)subs.get(1)).token.tokenType, STRING_KW );
        assertEquals( ((Primitive)subs.get(2)).token.tokenType, INTEGER_KW );
        array = ((Array)subs.get( 3 ));
        assertEquals( array.subordinates.size(), 2 );
        subs = (ArrayList)array.subordinates;
        assertEquals( ((Primitive)subs.get(0)).token.tokenType, INTEGER_KW );
        assertEquals( ((Primitive)subs.get(1)).token.tokenType, STRING_KW );
    }

    @Test
    public void testSavedTokens()
    {
        Tokenizer tokenizer = Tokenizer.getJCRTokenizer();
        tokenizer.tokenize( "[ integer, string ]" );
        /*
        ;comment
        [ integer, string ]
         */
        String s;
        s = "        ;comment\n" +
            "        [ integer, string ] ";
        tokenizer.tokenize( s );
        ArrayList<Token> tokens = tokenizer.getTokens();
        JcrParser jcrParser = new JcrParser();
        jcrParser.setupParse( tokens );

        ArrayList<Token> savedTokens = new ArrayList<>();
        class TestConsumer implements Consumer<ArrayList<Token>>
        {
            @Override
            public void accept( ArrayList<Token> tokens )
            {
                savedTokens.addAll( tokens );
            }
        }

        TestPrimitive tp = new TestPrimitive();
        boolean retval = jcrParser.sequence( Repeat.ONCE,
            new TestConsumer(),
            TokenType.SPACES,
            TokenType.COMMENT,
            TokenType.SPACES,
            TokenType.LEFT_BRACKET,
            TokenType.SPACES,
            TokenType.INTEGER_KW,
            TokenType.COMMA,
            TokenType.SPACES,
            TokenType.STRING_KW,
            TokenType.SPACES,
            TokenType.RIGHT_BRACKET,
            TokenType.SPACES );

        assertTrue( retval );
        assertEquals( savedTokens.size(), 5 );
        assertEquals( savedTokens.get( 0 ).tokenType, TokenType.LEFT_BRACKET );
        assertEquals( savedTokens.get( 1 ).tokenType, TokenType.INTEGER_KW );
        assertEquals( savedTokens.get( 2 ).tokenType, TokenType.COMMA );
        assertEquals( savedTokens.get( 3 ).tokenType, TokenType.STRING_KW );
        assertEquals( savedTokens.get( 4 ).tokenType, TokenType.RIGHT_BRACKET );
    }

    @Test
    public void testParserFailure()
    {
        expectThrows( ParsingException.class, () -> JcrParser.parse( "[integer] [integer,[ integer, string, foo ] " ) );
    }
}
