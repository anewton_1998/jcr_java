/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static org.testng.Assert.*;

/**
 * Tests for parsing and printing array rules.
 * There are a lot of tests for array rules in {@link NonTerminalsTest} so this test class isn't intended to
 * be comprehensive. It does however follow the toString method of testing used in {@link ObjectRuleRuleCreationTest}
 * and {@link GroupRuleCreationTest}.
 */
public class ArrayRuleRuleCreationTest
{
    @BeforeClass
    public void setup()
    {
        Level level = Level.INFO;
        //Level level = Level.FINE;
        //Level level = Level.FINEST;
        Logger rootLogger = Logger.getLogger("" );
        for ( Handler handler : rootLogger.getHandlers() )
        {
            handler.setLevel( level );
        }
        rootLogger.setLevel( level );

    }

    @AfterClass
    public void tearDown()
    {
        LogManager.getLogManager().reset();
    }

    @Test
    public void testArrayAnnotations()
    {
        // [ @{not} [ string ] ]
        RuleSet r = JcrParser.parse( "[ @{not} [ string ] ]" );
        assertEquals( r.getSub(0).toString(), "[ @{not} [ string ] ]" );
        assertNotNull( ((ArrayRule)r.getSub(0).getSub(0).getSub(0)).not );
    }

    @Test
    public void testEmptyArray()
    {
        // [ ]
        RuleSet r = JcrParser.parse( "[ ]" );
        assertEquals( r.getSub(0).toString(), "[ ]" );
    }

    @Test
    public void testArrayWithRegex()
    {
        // [ /^bar/ ]
        RuleSet r = JcrParser.parse( "[ /^bar/ ]" );
        assertEquals( r.getSub(0).toString(), "[ /^bar/ ]" );
    }

    @Test
    public void testArrayWithRepetitions()
    {
        // [ /^bar/ ? , /^foo/ *1..5 ]
        RuleSet r = JcrParser.parse( "[ /^bar/ ? , /^foo/ *1..5 ]" );
        assertEquals( r.getSub(0).toString(), "[ /^bar/ ? , /^foo/ *1..5 ]" );

        // [ /^bar/ ? | /^foo/ *1..5 ]
        r = JcrParser.parse( "[ /^bar/ ? | /^foo/ *1..5 ]" );
        assertEquals( r.getSub(0).toString(), "[ /^bar/ ? | /^foo/ *1..5 ]" );

        // [ /^bar/ ? | /^foo/ *1..5 | 100 ]
        r = JcrParser.parse( "[ /^bar/ ? | /^foo/ *1..5 | 100 ]" );
        assertEquals( r.getSub(0).toString(), "[ /^bar/ ? | /^foo/ *1..5 | 100 ]" );
    }
}
