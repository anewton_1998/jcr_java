/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static org.testng.Assert.assertEquals;

/**
 * Tests the parsing and printing of the JCR version directive.
 * Unlike the previous generation of parsing tests, these tests can rely on toString
 * because the ToStringTest has proven out the previous aspects of the object hierachy.
 * Using toString should make the tests more readable and easier to maintain.
 */
public class JcrVersionDirectiveTest
{
    @BeforeClass
    public void setup()
    {
        Level level = Level.INFO;
        //Level level = Level.FINE;
        //Level level = Level.FINEST;
        Logger rootLogger = Logger.getLogger("" );
        for ( Handler handler : rootLogger.getHandlers() )
        {
            handler.setLevel( level );
        }
        rootLogger.setLevel( level );

    }

    @AfterClass
    public void tearDown()
    {
        LogManager.getLogManager().reset();
    }

    @Test
    public void testJustVersion()
    {
        // # jcr-version 0.7
        RuleSet r = JcrParser.parse( "# jcr-version 0.7" );
        assertEquals( r.getSub(0).toString(), "# jcr-version 0.7" );
    }

    @Test
    public void testVersionWithExtensions()
    {
        /*
          There is some sorta weird thing with the parser about putting NAME tokens in reverse order
          on the stack for things on the same line. This would be more orderly if we used accept()
          but it isn't a big deal. Not getting wrapped around the axle here.
         */

        // # jcr-version 0.7 + strawberry + blueberry
        RuleSet r = JcrParser.parse( "# jcr-version 0.7 + blueberry + strawberry" );
        assertEquals( r.getSub(0).toString(), "# jcr-version 0.7 + blueberry + strawberry" );

        /*
        #{ jcr-version 0.7
           + blueberry
           + strawberry }
        */
        r = JcrParser.parse( "#{ jcr-version 0.7\n" +
                             "  + blueberry\n" +
                             "  + strawberry }" );
        assertEquals( r.getSub(0).toString(), "# jcr-version 0.7 + blueberry + strawberry" );
    }

    @Test( expectedExceptions = { ParsingException.class } )
    public void testBadVersion()
    {
        // # jcr-version 0.1
        RuleSet r = JcrParser.parse( "# jcr-version 0.1" );
    }

}
