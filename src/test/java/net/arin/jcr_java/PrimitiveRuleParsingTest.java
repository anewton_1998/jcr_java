/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * Test for parsing primitives.
 */
public class PrimitiveRuleParsingTest
{
    @BeforeClass
    public void setup()
    {
        Level level = Level.INFO;
        //Level level = Level.FINE;
        Logger rootLogger = Logger.getLogger( "" );
        for ( Handler handler : rootLogger.getHandlers() )
        {
            handler.setLevel( level );
        }
        rootLogger.setLevel( level );

    }

    @AfterClass
    public void tearDown()
    {
        LogManager.getLogManager().reset();
    }

    @Test
    public void testQString()
    {
        RuleSet ruleSet = JcrParser.parse( "\"a q string\"" );
        assertNotNull( ruleSet );
        assertEquals( ruleSet.ruleSetSubordinates.size(), 1 );
        PrimitiveRule p = (PrimitiveRule) ruleSet.ruleSetSubordinates.iterator().next();
        assertEquals( p.tokens.get(0).sequence, "\"a q string\"" );
    }

    @Test
    public void testRegex()
    {
        RuleSet ruleSet = JcrParser.parse( "/aaabbb?/" );
        assertNotNull( ruleSet );
        assertEquals( ruleSet.ruleSetSubordinates.size(), 1 );
        PrimitiveRule p = (PrimitiveRule) ruleSet.ruleSetSubordinates.iterator().next();
        assertEquals( p.tokens.get(0).sequence, "/aaabbb?/" );
    }

    @Test
    public void testSimpleParsing()
    {
        //with some of these, there isn't a whole lot to be done other than
        //to see that an exception is not thrown.
        /*
        0
        1
        -1
        0..
        0..1
        -1..0
        -1..1
        ..0
        ..-1
        ..1
        0.0
        0.1
        1.0
        -1.0
        0.1..
        0.1..0.2
        -0.1..0.1
        ..-0.1
        ..0.1
        int8
        int16
        int32
        int64
        uint8
        uint16
        uint32
        uint64
        hex
        base32hex
        base32
        base64url
        base64
        any
        fqdn
        idn
        email
        phone
        true
        false
        boolean
        null
        ipv6
        ipv4
        ipaddr
        double
        float
        date
        time
        datetime
        uri
        uri..https
         */
        String s;
        s = "        0\n" +
            "        1\n" +
            "        -1\n" +
            "        0..\n" +
            "        0..1\n" +
            "        -1..0\n" +
            "        -1..1\n" +
            "        ..0\n" +
            "        ..-1\n" +
            "        ..1\n" +
            "        0.0\n" +
            "        0.1\n" +
            "        1.0\n" +
            "        -1.0\n" +
            "        0.1..\n" +
            "        0.1..0.2\n" +
            "        -0.1..0.1\n" +
            "        ..-0.1\n" +
            "        ..0.1\n" +
            "        int8\n" +
            "        int16\n" +
            "        int32\n" +
            "        int64\n" +
            "        uint8\n" +
            "        uint16\n" +
            "        uint32\n" +
            "        uint64\n" +
            "        hex\n" +
            "        base32hex\n" +
            "        base32\n" +
            "        base64url\n" +
            "        base64\n" +
            "        any\n" +
            "        fqdn\n" +
            "        idn\n" +
            "        email\n" +
            "        phone\n" +
            "        true\n" +
            "        false\n" +
            "        boolean\n" +
            "        null\n" +
            "        ipv6\n" +
            "        ipv4\n" +
            "        ipaddr\n" +
            "        double\n" +
            "        float\n" +
            "        date\n" +
            "        time\n" +
            "        datetime\n" +
            "        uri\n" +
            "        uri..https";
        RuleSet ruleSet = JcrParser.parse( s );
    }
}
