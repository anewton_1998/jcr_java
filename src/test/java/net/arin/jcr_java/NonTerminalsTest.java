/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.swing.*;
import java.util.Iterator;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertNull;

/**
 * Some tests of rule set parsing and so forth.
 */
public class NonTerminalsTest
{
    @BeforeClass
    public void setup()
    {
        Level level = Level.INFO;
        //Level level = Level.FINE;
        //Level level = Level.FINEST;
        Logger rootLogger = Logger.getLogger( "" );
        for ( Handler handler : rootLogger.getHandlers() )
        {
            handler.setLevel( level );
        }
        rootLogger.setLevel( level );

    }

    @AfterClass
    public void tearDown()
    {
        LogManager.getLogManager().reset();
    }

    @Test
    public void testRuleSet()
    {
        /*
        ; a rule set
        [ integer ]
         */
        String s;
        s = "        ; a rule set\n" +
            "        [ integer ]";
        RuleSet ruleSet = JcrParser.parse( s );
        assertNotNull( ruleSet );
        assertEquals( ruleSet.ruleSetSubordinates.size(), 1 );
        ArrayRule arrayRule = (ArrayRule)ruleSet.ruleSetSubordinates.iterator().next();
        assertEquals(arrayRule.arraySubordinates.size(), 1 );
        ContainedItem containedItem = ( ContainedItem ) arrayRule.arraySubordinates.iterator().next();
        PrimitiveRule primitiveRule = (PrimitiveRule) containedItem.jsonContentRule;
        assertEquals(primitiveRule.tokens.get(0).tokenType, TokenType.INTEGER_KW );
    }

    @Test
    public void test2LevelsOfArray()
    {
        /*
        ; a rule set
        [integer , [integer]]
         */
        String s;
        s = "        ; a rule set\n" +
            "        [integer , [integer]]";
        RuleSet ruleSet = JcrParser.parse( s );
        assertNotNull( ruleSet );
        assertEquals( ruleSet.ruleSetSubordinates.size(), 1 );
        ArrayRule arrayRule = (ArrayRule)ruleSet.ruleSetSubordinates.iterator().next();
        assertEquals(arrayRule.arraySubordinates.size(), 2 );
        Iterator<ParsedObject> topArrayIterator = arrayRule.arraySubordinates.iterator();
        ContainedItem containedItem = ( ContainedItem ) topArrayIterator.next();
        PrimitiveRule primitiveRule = (PrimitiveRule) containedItem.jsonContentRule;
        assertEquals(primitiveRule.tokens.get(0).tokenType, TokenType.INTEGER_KW );
        containedItem = ( ContainedItem ) topArrayIterator.next();
        arrayRule = (ArrayRule) containedItem.jsonContentRule;
        containedItem = ( ContainedItem ) arrayRule.arraySubordinates.iterator().next();
        primitiveRule = (PrimitiveRule) containedItem.jsonContentRule;
        assertEquals(primitiveRule.tokens.get(0).tokenType, TokenType.INTEGER_KW );
    }

    @Test
    public void testAnnotations()
    {
        /*
        ; a rule set
        @{not} [integer,@{unordered} [ @{not} integer]]
         */
        String s;
        s = "        ; a rule set\n" +
            "        @{not} [integer,@{unordered} [ @{not} integer]]";
        RuleSet ruleSet = JcrParser.parse( s );
        assertNotNull( ruleSet );
        assertEquals( ruleSet.ruleSetSubordinates.size(), 1 );
        ArrayRule arrayRule = (ArrayRule)ruleSet.ruleSetSubordinates.iterator().next();
        assertNotNull(arrayRule.not );
        assertEquals(arrayRule.arraySubordinates.size(), 2 );
        Iterator<ParsedObject> topArrayIterator = arrayRule.arraySubordinates.iterator();
        ContainedItem containedItem = ( ContainedItem ) topArrayIterator.next();
        PrimitiveRule primitiveRule = (PrimitiveRule) containedItem.jsonContentRule;
        assertEquals(primitiveRule.tokens.get(0).tokenType, TokenType.INTEGER_KW );
        containedItem = ( ContainedItem ) topArrayIterator.next();
        arrayRule = (ArrayRule) containedItem.jsonContentRule;
        assertNotNull(arrayRule.unordered );
        containedItem = ( ContainedItem ) arrayRule.arraySubordinates.iterator().next();
        primitiveRule = (PrimitiveRule) containedItem.jsonContentRule;
        assertEquals(primitiveRule.tokens.get(0).tokenType, TokenType.INTEGER_KW );
        assertNotNull(primitiveRule.not );
    }

    @Test
    public void testRepetition()
    {
        RuleSet r;
        r = JcrParser.parse( "[ integer *2 ]" );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.min, 2 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.max, 2 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.step, 1 );
        r = JcrParser.parse( "[ integer *2]" );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.min, 2 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.max, 2 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.step, 1 );
        r = JcrParser.parse( "[ integer *2, string ? ]" );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.min, 2 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.max, 2 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.step, 1 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 1 )).repetition.min, 0 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 1 )).repetition.max, 1 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 1 )).repetition.step, 1 );
        r = JcrParser.parse( "[ integer ? ]" );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.min, 0 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.max, 1 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.step, 1 );
        r = JcrParser.parse( "[ integer + ]" );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.min, 1 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.max, Integer.MAX_VALUE );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.step, 1 );
        r = JcrParser.parse( "[ integer * ]" );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.min, 0 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.max, Integer.MAX_VALUE );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.step, 1 );
        r = JcrParser.parse( "[ integer *2.. ]" );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.min, 2 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.max, Integer.MAX_VALUE );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.step, 1 );
        r = JcrParser.parse( "[ integer *..2 ]" );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.min, 0 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.max, 2 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.step, 1 );
        r = JcrParser.parse( "[ integer *1..2 ]" );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.min, 1 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.max, 2 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.step, 1 );
        r = JcrParser.parse( "[ integer +%2 ]" );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.min, 1 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.max, Integer.MAX_VALUE );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.step, 2 );
        r = JcrParser.parse( "[ integer *%2 ]" );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.min, 0 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.max, Integer.MAX_VALUE );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.step, 2 );
        r = JcrParser.parse( "[ integer *2..%2 ]" );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.min, 2 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.max, Integer.MAX_VALUE );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.step, 2 );
        r = JcrParser.parse( "[ integer *..2%2 ]" );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.min, 0 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.max, 2 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.step, 2 );
        r = JcrParser.parse( "[ integer *1..2%2 ]" );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.min, 1 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.max, 2 );
        assertEquals( ((ContainedItem)r.getSub( 0 ).getSub( 0 )).repetition.step, 2 );
    }

    @Test
    public void testTargetRuleNames()
    {
        RuleSet r;

        r = JcrParser.parse( "[$foo]" );
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 0 ).getSub( 0 )).targetRuleName, "foo");
        r = JcrParser.parse( "[ $foo ]" );
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 0 ).getSub( 0 )).targetRuleName, "foo");
        r = JcrParser.parse( "[ $foo, integer ]" );
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 0 ).getSub( 0 )).targetRuleName, "foo");
        assertTrue( r.getSub( 0 ).getSub( 1 ).getSub( 0 ) instanceof PrimitiveRule);
        r = JcrParser.parse( "[ $foo, integer, $foo ]" );
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 0 ).getSub( 0 )).targetRuleName, "foo");
        assertTrue( r.getSub( 0 ).getSub( 1 ).getSub( 0 ) instanceof PrimitiveRule);
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 2 ).getSub( 0 )).targetRuleName, "foo");

        r = JcrParser.parse( "[$bar.foo]" );
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 0 ).getSub( 0 )).ruleSetAlias, "bar");
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 0 ).getSub( 0 )).targetRuleName, "foo");
        r = JcrParser.parse( "[ $bar.foo ]" );
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 0 ).getSub( 0 )).ruleSetAlias, "bar");
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 0 ).getSub( 0 )).targetRuleName, "foo");
        r = JcrParser.parse( "[ $bar.foo, integer ]" );
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 0 ).getSub( 0 )).ruleSetAlias, "bar");
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 0 ).getSub( 0 )).targetRuleName, "foo");
        assertTrue( r.getSub( 0 ).getSub( 1 ).getSub( 0 ) instanceof PrimitiveRule);
        r = JcrParser.parse( "[ $bar.foo, integer, $baz.foo ]" );
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 0 ).getSub( 0 )).ruleSetAlias, "bar");
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 0 ).getSub( 0 )).targetRuleName, "foo");
        assertTrue( r.getSub( 0 ).getSub( 1 ).getSub( 0 ) instanceof PrimitiveRule);
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 2 ).getSub( 0 )).ruleSetAlias, "baz");
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 2 ).getSub( 0 )).targetRuleName, "foo");

        r = JcrParser.parse( "[@{not}$foo]" );
        assertNotNull( ((TargetRuleName)r.getSub( 0 ).getSub( 0 ).getSub( 0 )).not );
        assertEquals( ((TargetRuleName)r.getSub( 0 ).getSub( 0 ).getSub( 0 )).targetRuleName, "foo");
    }

    @Test
    public void testNamedRule()
    {
        RuleSet r;
        r = JcrParser.parse( "$foo = [ integer ]" );
        assertEquals( ((NamedRule)r.getSub( 0 )).name, "foo" );
        assertTrue( r.getSub( 0 ).getSub( 0 ) instanceof ArrayRule);

        r = JcrParser.parse( "@{root} $foo = [ integer ]" );
        assertEquals( ((NamedRule)r.getSub( 0 )).name, "foo" );
        assertTrue( r.getSub( 0 ).getSub( 0 ) instanceof ArrayRule);
        assertNotNull( ((NamedRule)r.getSub( 0 )).root );
        assertNull( ((NamedRule)r.getSub( 0 )).not );
    }

    @Test
    public void testMemberRule()
    {
        RuleSet r;

        // $foo = /^bar/ : integer
        r = JcrParser.parse( "$foo = /^bar/ : integer" );
        assertEquals( ((NamedRule)r.getSub(0)).name, "foo" );
        assertTrue( r.getSub(0).getSub(0) instanceof MemberRule);
        assertEquals(((MemberRule)r.getSub(0).getSub(0)).memberRegex , "/^bar/" );
        assertEquals(((PrimitiveRule)r.getSub(0).getSub(0).getSub(0)).tokens.get(0).tokenType , TokenType.INTEGER_KW );

        // $foo = "bar" : integer
        r = JcrParser.parse( "$foo = \"bar\" : integer" );
        assertEquals( ((NamedRule)r.getSub(0)).name, "foo" );
        assertTrue( r.getSub(0).getSub(0) instanceof MemberRule);
        assertEquals(((MemberRule)r.getSub(0).getSub(0)).memberName , "\"bar\"" );
        assertEquals(((PrimitiveRule)r.getSub(0).getSub(0).getSub(0)).tokens.get(0).tokenType , TokenType.INTEGER_KW );

        // $foo = @{not} "bar" : integer
        r = JcrParser.parse( "$foo = @{not} \"bar\" : integer" );
        assertEquals( ((NamedRule)r.getSub(0)).name, "foo" );
        assertTrue( r.getSub(0).getSub(0) instanceof MemberRule);
        assertNotNull( ((MemberRule)r.getSub(0).getSub(0)).not );
        assertEquals(((MemberRule)r.getSub(0).getSub(0)).memberName , "\"bar\"" );
        assertEquals(((PrimitiveRule)r.getSub(0).getSub(0).getSub(0)).tokens.get(0).tokenType , TokenType.INTEGER_KW );
    }

    /**
     * Just testing the parsing of the grammar, not the structures created afterward
     */
    @Test
    public void testOneLineDirectivesParsing()
    {
        /*
        # jcr-version 0.7 + blueberry + strawberry
        # ruleset-id funky_stuff
        # import other_funk as funkier
        [ string ]
        */
        String s =
                "# jcr-version 0.7 + blueberry + strawberry\n" +
                "# ruleset-id funky_stuff\n" +
                "# import other_funk as funkier\n" +
                "[ string ]";
        JcrParser.parse( s );
    }

    /**
     * Just testing the parsing of the grammar, not the structures created afterward
     */
    @Test
    public void testMultiLineDirectivesParsing()
    {
        /*
        #{ jcr-version 0.7
           + blueberry
           + strawberry }
        #{ ruleset-id
             funky_stuff }
        #{ import
             other_funk
           as funkier }
        [ string ]
        */
        String s =
                "#{ jcr-version 0.7 \n" +
                "   + blueberry \n" +
                "   + strawberry }\n" +
                "#{ ruleset-id \n" +
                "     funky_stuff }\n" +
                "#{ import \n" +
                "     other_funk \n" +
                "   as funkier }\n" +
                "[ string ]";
        JcrParser.parse( s );
    }


}
