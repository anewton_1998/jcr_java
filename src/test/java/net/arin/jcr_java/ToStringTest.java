/*
 * This software and all files contained in it are distrubted under the MIT license.
 *
 * Copyright (C) 2016-2017 American Registry for Internet Numbers (ARIN)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.arin.jcr_java;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Tests for toString usage.
 * This is just here so we can see how things look on the console.
 * This really isn't a test. Should be disabled normally.
 */
public class ToStringTest
{
    @Test( enabled = false )
    public void testToString()
    {
        /*
        ;a comment
        [ integer ] [ string *2 ]
        $foo = [ "foo", @{not} "bar" ]
         */
        String s;
        s = "        ;a comment\n" +
            "        [ integer ] [ string *2 ]\n" +
            "        $foo = [ \"foo\", @{not} \"bar\" ]";
        RuleSet r;
        r = JcrParser.parse( s );
        System.out.println( r.toString() );
    }

    @Test
    public void testAnnotationsToString()
    {
        RuleSet r;
        String s;

        r = JcrParser.parse( "@{root}@{not} $foo = [ integer ] " );
        s = r.getSub(0).toString();
        assertEquals( s.substring(0, s.indexOf( '$' )) , "@{not} @{root} ");

        r = JcrParser.parse( "@{root} $foo = [ integer ] " );
        s = r.getSub(0).toString();
        assertEquals( s.substring(0, s.indexOf( '$' )) , "@{root} ");

        r = JcrParser.parse( "$foo = [ integer ] " );
        s = r.getSub(0).toString();
        assertEquals( s.substring(0, s.indexOf( '$' )) , "");
    }

    @Test
    public void testArrayToString()
    {
        RuleSet r;

        r = JcrParser.parse( "[integer] " );
        assertEquals( r.getSub(0).toString(), "[ integer ]");

        r = JcrParser.parse( "[integer,string] " );
        assertEquals( r.getSub(0).toString(), "[ integer , string ]");

        r = JcrParser.parse( "[integer,[string]] " );
        assertEquals( r.getSub(0).toString(), "[ integer , [ string ] ]");
    }

    @Test
    public void testRepetitionToString()
    {
        RuleSet r;

        r = JcrParser.parse( "[integer] " );
        assertEquals( r.getSub(0).toString(), "[ integer ]");

        r = JcrParser.parse( "[integer+] " );
        assertEquals( r.getSub(0).toString(), "[ integer + ]");

        r = JcrParser.parse( "[integer*] " );
        assertEquals( r.getSub(0).toString(), "[ integer * ]");

        r = JcrParser.parse( "[integer?] " );
        assertEquals( r.getSub(0).toString(), "[ integer ? ]");

        r = JcrParser.parse( "[integer*1..2] " );
        assertEquals( r.getSub(0).toString(), "[ integer *1..2 ]");

        r = JcrParser.parse( "[integer*1..2%3] " );
        assertEquals( r.getSub(0).toString(), "[ integer *1..2%3 ]");

        r = JcrParser.parse( "[integer*0..1] " );
        assertEquals( r.getSub(0).toString(), "[ integer ? ]");
    }

    @Test
    public void testMemberToString()
    {
        RuleSet r;


        // $foo = "bar" : integer
        r = JcrParser.parse( "$foo = \"bar\" : integer" );
        assertEquals( r.getSub(0).toString(), "$foo = \"bar\" : integer");

        // $foo = @{not} "bar" : integer
        r = JcrParser.parse( "$foo = @{not} \"bar\" : integer" );
        assertEquals( r.getSub(0).toString(), "$foo = @{not} \"bar\" : integer");

        // $foo = @{not} /^bar/ : integer
        r = JcrParser.parse( "$foo = @{not} /^bar/ : integer" );
        assertEquals( r.getSub(0).toString(), "$foo = @{not} /^bar/ : integer");

        // $foo = @{not} /^bar/ : [ integer * ]
        r = JcrParser.parse( "$foo = @{not} /^bar/ : [ integer * ]" );
        assertEquals( r.getSub(0).toString(), "$foo = @{not} /^bar/ : [ integer * ]");
    }

    @Test
    public void testTargetRuleNameToString()
    {
        RuleSet r;

        r = JcrParser.parse( "[ $foo ]" );
        assertEquals( r.getSub(0).toString(), "[ $foo ]");

        r = JcrParser.parse( "[ $foo.bar ]" );
        assertEquals( r.getSub(0).toString(), "[ $foo.bar ]");

        r = JcrParser.parse( "[ @{not}$foo ]" );
        assertEquals( r.getSub(0).toString(), "[ @{not} $foo ]");

        r = JcrParser.parse( "[ @{not}$foo.bar ]" );
        assertEquals( r.getSub(0).toString(), "[ @{not} $foo.bar ]");
    }

    @Test
    public void testRegexAndQStringPrimitiveToString()
    {
        RuleSet r;

        // $foo = "bar" : "foo"
        r = JcrParser.parse( "$foo = \"bar\" : \"foo\"" );
        assertEquals( r.getSub(0).toString(), "$foo = \"bar\" : \"foo\"" );

        // $foo = "bar" : /^foo/
        r = JcrParser.parse( "$foo = \"bar\" : /^foo/" );
        assertEquals( r.getSub(0).toString(), "$foo = \"bar\" : /^foo/" );
    }

    @Test
    public void testPrimitiveStaticToStrings()
    {
        RuleSet r;

        r = JcrParser.parse( "[ float , double , integer , true , false , boolean , null ]");
        assertEquals( r.getSub(0).toString(), "[ float , double , integer , true , false , boolean , null ]");

        r = JcrParser.parse( "[ hex , base32 , base32hex , base64 , base64url ]");
        assertEquals( r.getSub(0).toString(), "[ hex , base32 , base32hex , base64 , base64url ]");

        r = JcrParser.parse( "[ email , phone , fqdn , idn , ipv6 , ipv4 , ipaddr ]");
        assertEquals( r.getSub(0).toString(), "[ email , phone , fqdn , idn , ipv6 , ipv4 , ipaddr ]");

        r = JcrParser.parse( "[ date , time , datetime , any , null ]");
        assertEquals( r.getSub(0).toString(), "[ date , time , datetime , any , null ]");
    }

    @Test
    public void testPrimitiveIntUintSizesToString()
    {
        RuleSet r;

        //just pick some numbers... replicating the algorithm is just a test of copying code if you ask me

        r = JcrParser.parse( "[ uint8 ]" );
        assertEquals( r.getSub(0).toString(), "[ 0..256 ]");

        r = JcrParser.parse( "[ int8 ]" );
        assertEquals( r.getSub(0).toString(), "[ -127..128 ]");

        r = JcrParser.parse( "[ uint16 ]" );
        assertEquals( r.getSub(0).toString(), "[ 0..65536 ]");

        r = JcrParser.parse( "[ int16 ]" );
        assertEquals( r.getSub(0).toString(), "[ -32767..32768 ]");
    }

    @Test
    public void testPrimitiveFloatRangesToString()
    {
        RuleSet r;

        r = JcrParser.parse( "[ 100.01 ]" );
        assertEquals( r.getSub(0).toString(), "[ 100.01 ]");

        r = JcrParser.parse( "[ 100.01..101.02 ]" );
        assertEquals( r.getSub(0).toString(), "[ 100.01..101.02 ]");

        r = JcrParser.parse( "[ -100.01..101.02 ]" );
        assertEquals( r.getSub(0).toString(), "[ -100.01..101.02 ]");

        //
        // it will be interesting to see how these works across JVMs and OS platforms
        //

        r = JcrParser.parse( "[ ..100.01 ]" );
        assertEquals( r.getSub(0).toString(), "[ 1.4E-45..100.01 ]");

        r = JcrParser.parse( "[ 100.01.. ]" );
        assertEquals( r.getSub(0).toString(), "[ 100.01..3.4028235E38 ]");
    }

    @Test
    public void testPrimtiiveIntegerRangesToString()
    {
        RuleSet r;

        r = JcrParser.parse( "[ 100 ]" );
        assertEquals( r.getSub(0).toString(), "[ 100 ]");

        r = JcrParser.parse( "[ 100..101 ]" );
        assertEquals( r.getSub(0).toString(), "[ 100..101 ]");

        r = JcrParser.parse( "[ -100..101 ]" );
        assertEquals( r.getSub(0).toString(), "[ -100..101 ]");

        r = JcrParser.parse( "[ ..100 ]" );
        assertEquals( r.getSub(0).toString(), "[ -2147483648..100 ]");

        r = JcrParser.parse( "[ 100.. ]" );
        assertEquals( r.getSub(0).toString(), "[ 100..9223372036854775807 ]");
    }

    @Test
    public void testPrimitiveUriToString()
    {
        RuleSet r;

        r = JcrParser.parse( "[ uri ]" );
        assertEquals( r.getSub(0).toString(), "[ uri ]");

        r = JcrParser.parse( "[ uri..https ]" );
        assertEquals( r.getSub(0).toString(), "[ uri..https ]");
    }
}
