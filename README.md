# JCR Java

A Java implementation of [JSON Content Rules (JCR)](http://codalogic.github.io/jcr/).

### Goals

A faithful Java implementation of JCR. 

If done right, this could hopefully become the replacement reference
implementation. The current reference implementation is [JCRValidator](https://github.com/arineng/jcrvalidator), 
a Ruby implementation. JCRValidator is a Ruby implementation that uses Parslet, a PEG parser, to parse the JCR
syntax.

This implementation uses a hand-built parser that more closely follows the official 
[ABNF](https://tools.ietf.org/html/draft-newton-json-content-rules-08#section-7).

It is also hoped that this implementation can be more performant for JSON evaluations by using Java 8 lambdas
and Java threads. Therefore this is targeting Java 8 (and above).

### Current Status

This is in the very preliminary stages, and is not at all usable.

At present, the parser framework is laid out and parsing most of the basic JCR syntax. The following are a list of
tasks left to do:

* parsing of directives
* mapping of target rule names
* checking of subgroups to arrays and objects
* validation of primitives
* validation of arrays
* validation of members
* validation of groups
* ...
* tie it altogether

### Building

Java 8 is required to build and run the tests. Specifically, a JDK (not just a JRE). 
Installing that is platform dependent, and if you have other versions
of Java installed you'll need to understand how to run multiple versions of Java simultaneously (some systems have
various means for this ). At the very least, `java` and `javac` should be in your shell's executable path and
it may be required that your shell have a JAVA_HOME environment variable pointing the installation of the Java 8 JDK.

From there, building and running the tests is as follows.

For Linux, MacOS and Unix styled OS, use the Gradle wrapper shell script:

```
./gradlew clean build
```

For Windows, use the Gradle wrapper batch script:

```
gradlew.bat clean build

```

### Background

For more information on JCR:

* IETF Internet-Drraft specification [draft-newton-json-content-rules-08](https://tools.ietf.org/html/draft-newton-json-content-rules-08)
* [JSON Content Rules](https://json-content-rules.org) web page
* The [JCRValidator](https://github.com/arineng/jcrvalidator) Ruby implementation on GitHub
* Codalogic's [C++ JCR Parser](https://github.com/codalogic/cl-jcr-parser) on GitHub
* The [IETF Internet-Draft GitHub](https://github.com/arineng/jcr) page.
* contact@json-content-rules.org

